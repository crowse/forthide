/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shorai.forthide;

import javax.swing.JTree;

/**
 *  Node to represent a core MeCrisp word 
 *        wtag and ctag are NOT present
 * 
 * Mecrisp vocabularies are loaded as an extension, they add a 2 cell prefix in 
 * the dictionary that defines the CFA of the containing vocabulary.
 * This field(s) are not available in the core implementation
 * These words are defined in the initial flash image cross assembled and flashed
 * 
 * @author Chris
 */
public class LoadedFileNode extends AbstractWordNode {
    
    public LoadedFileNode(String name, Object children) {
        super(name,children);
    }
    
    
    
    
    
}
