/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shorai.forthide;

import javax.swing.JTree;

/**
 *  Node to represent a core word wtag and ctag are NOT present
 * These are the word flashed to the device from the MeCrisp installation
 * @TODO: look for specific words and add them to subtrees.
 * 
 * This will make HELP much simpler
 * 
 * Use the Mecrisp Glossary to define the tree
 * 
 * @author Chris
 */
public class CoreWordNode extends AbstractWordNode {
    
    public CoreWordNode(String name, Object children) {
        super(name,children);
    }
    
}
