/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shorai.forthide;

import javax.swing.JTree;

/**
 *  Subclass of Tree Node for Vocabularies
 *  This subclassing allows us to assign specific icons and colours based on class
 *  It also provides better handling of trees and branches
 * 
 * Implementation is specific to MeCrisp experimental vocabulary handling
 * 
 * @author Chris
 */
public class VocabularyNode extends AbstractWordNode {
    
    public VocabularyNode(String name, Object children) {
        super(name,children);
    }
    
}
