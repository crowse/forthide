/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shorai.forthide;

import javax.swing.JTree;

/**
 *  Node to represent a core word wtag and ctag are NOT present
 * @author Chris
 */
public class ExtensionWordNode extends CoreWordNode {
    
    int wtag;
    int ctag;
    
    
    public ExtensionWordNode(String name, Object children) {
        super(name,children);
    }
    
}
