/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shorai.forthide;

import com.fazecast.jSerialComm.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import static java.lang.Boolean.TRUE;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JTree;
import javax.swing.ListModel;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;



/**
 *
 * @author Chris
 */
public class ForthIDEMain extends javax.swing.JFrame {

    static ForthIDEMain fm;

    SerialPort[] ports;

    SerialPort port;

    InputStream inputStream;
    OutputStream outputStream;

    byte buffer[] = new byte[2004];
    StringBuilder sb = new StringBuilder();

    String txTerm = "\n";

    //  List<String> allwords = new ArrayList();
    //DefaultListModel allwordsModel = new DefaultListModel<String>();
    JTree.DynamicUtilTreeNode allwordsTree = new JTree.DynamicUtilTreeNode("Forth", null);
    DefaultTreeModel allwordsTreeModel = new DefaultTreeModel(allwordsTree);
    // TODO: try SortableTreeModel stm = new SortaleTreeModel(allwordsTreeModel); 

    //  List<String> history;
    //  ComboBoxModel<String> historyModel=new ComboBoxModel(history);
    /**
     * Creates new form ForthIDEMain
     */
    public ForthIDEMain() {

        int i = 0;
        ports = SerialPort.getCommPorts();
        if (ports.length < 1) {
            System.out.println("No Serial ports are attached and avaailbale");
            System.exit(1);
        }

        port = ports[0];
        port.setComPortParameters(115200, 8, SerialPort.ONE_STOP_BIT, SerialPort.NO_PARITY);
        port.openPort();

        System.out.println("Connected to port :" + port.getDescriptivePortName() + ":" + port.getSystemPortName());
        inputStream = port.getInputStream();
        outputStream = port.getOutputStream();

        port.addDataListener(new SerialPortDataListener() {
            @Override
            public int getListeningEvents() {
                return SerialPort.LISTENING_EVENT_DATA_WRITTEN;
            }

            @Override
            public void serialEvent(SerialPortEvent event) {
                if (event.getEventType() == SerialPort.LISTENING_EVENT_DATA_WRITTEN) {
                    System.out.println("All bytes were successfully transmitted!");
                }
                if (event.getEventType() == SerialPort.LISTENING_EVENT_DATA_RECEIVED) {
                    portRx();
                }
            }
        });

        i = 1;
        initComponents();
        // jList1.setModel(allwordsModel);
        jTree1.setModel(allwordsTreeModel);
        allwordsTree.setAllowsChildren(true);
    }

    void portTx(String str) {
        String strx = str; //System.lineSeparator();

        log("Tx:", strx);
        try {
            outputStream.write(strx.getBytes());
            outputStream.flush();
        } catch (Exception exc) {
            log("PortTx Exception:", exc.getLocalizedMessage());
        }
    }

    String portRx() {
        String ret = "ok.";
        if (inputStream == null) {
            ret = "null";
        } else {
            try {
                int len = inputStream.available();
                if (len > 0) {
                    if (len > 2000) {
                        len = 2000;
                    }
                    byte buffer[] = new byte[len + 1];
                    inputStream.read(buffer, 0, len);
                    buffer[len] = 0;
                    String str = new String(buffer);
                    log("Rx:", str);
                    if (!str.endsWith("ok.")) {
                        ret = str;
                    }
                }
            } catch (Exception exc) {
                ret = "Exception portRx:" + exc.getLocalizedMessage();
                log("Exception portRx:", exc.getLocalizedMessage());
            }
        }
        return ret;
    }

    void txRx(String str) {
        addHistory(str);
        if (!str.endsWith("\n")) {
            str += txTerm;
        }
        try {
            portTx(str);        // TODO add your handling code here:
            Thread.sleep(100);
            portRx();
        } catch (Exception exc) {
            log("TxRx:", exc.getLocalizedMessage());
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        desktopPane = new javax.swing.JDesktopPane();
        jPanel1 = new javax.swing.JPanel();
        jTextField1 = new javax.swing.JTextField();
        jComboBox1 = new javax.swing.JComboBox<>();
        jButton2 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTree1 = new javax.swing.JTree();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        menuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        openMenuItem = new javax.swing.JMenuItem();
        saveMenuItem = new javax.swing.JMenuItem();
        saveAsMenuItem = new javax.swing.JMenuItem();
        exitMenuItem = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        editMenu = new javax.swing.JMenu();
        cutMenuItem = new javax.swing.JMenuItem();
        copyMenuItem = new javax.swing.JMenuItem();
        pasteMenuItem = new javax.swing.JMenuItem();
        deleteMenuItem = new javax.swing.JMenuItem();
        helpMenu = new javax.swing.JMenu();
        contentMenuItem = new javax.swing.JMenuItem();
        aboutMenuItem = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        desktopPane.setAlignmentX(0.0F);

        jPanel1.setNextFocusableComponent(jPanel2);

        jTextField1.setText("(list)");
        jTextField1.setToolTipText("");
        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });

        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        jButton2.setText("Tx");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton1.setText("Rx");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextField1)
                    .addComponent(jComboBox1, 0, 751, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton2, javax.swing.GroupLayout.Alignment.TRAILING)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addContainerGap())
        );

        desktopPane.add(jPanel1);
        jPanel1.setBounds(10, 10, 810, 80);

        jTree1.setMaximumSize(new java.awt.Dimension(77, 999));
        jScrollPane3.setViewportView(jTree1);

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 288, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 480, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 259, Short.MAX_VALUE))
                .addGap(29, 29, 29))
        );

        desktopPane.add(jPanel2);
        jPanel2.setBounds(0, 80, 810, 300);

        fileMenu.setMnemonic('f');
        fileMenu.setText("File");

        openMenuItem.setMnemonic('o');
        openMenuItem.setText("Open");
        openMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(openMenuItem);

        saveMenuItem.setMnemonic('s');
        saveMenuItem.setText("Save");
        fileMenu.add(saveMenuItem);

        saveAsMenuItem.setMnemonic('a');
        saveAsMenuItem.setText("Save As ...");
        saveAsMenuItem.setDisplayedMnemonicIndex(5);
        fileMenu.add(saveAsMenuItem);

        exitMenuItem.setMnemonic('x');
        exitMenuItem.setText("Exit");
        exitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(exitMenuItem);

        jMenuItem1.setText("Reload Words");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        fileMenu.add(jMenuItem1);

        menuBar.add(fileMenu);

        editMenu.setMnemonic('e');
        editMenu.setText("Edit");

        cutMenuItem.setMnemonic('t');
        cutMenuItem.setText("Cut");
        editMenu.add(cutMenuItem);

        copyMenuItem.setMnemonic('y');
        copyMenuItem.setText("Copy");
        editMenu.add(copyMenuItem);

        pasteMenuItem.setMnemonic('p');
        pasteMenuItem.setText("Paste");
        editMenu.add(pasteMenuItem);

        deleteMenuItem.setMnemonic('d');
        deleteMenuItem.setText("Delete");
        editMenu.add(deleteMenuItem);

        menuBar.add(editMenu);

        helpMenu.setMnemonic('h');
        helpMenu.setText("Help");

        contentMenuItem.setMnemonic('c');
        contentMenuItem.setText("Contents");
        helpMenu.add(contentMenuItem);

        aboutMenuItem.setMnemonic('a');
        aboutMenuItem.setText("About");
        helpMenu.add(aboutMenuItem);

        jMenuItem2.setText(": (list)...;");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        helpMenu.add(jMenuItem2);

        menuBar.add(helpMenu);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(desktopPane, javax.swing.GroupLayout.DEFAULT_SIZE, 828, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(desktopPane, javax.swing.GroupLayout.DEFAULT_SIZE, 405, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitMenuItemActionPerformed
        System.exit(0);
    }//GEN-LAST:event_exitMenuItemActionPerformed

    void addHistory(String str) {
        jComboBox1.removeItem(str);
        jComboBox1.insertItemAt(str, 0);
        while (jComboBox1.getModel().getSize() > 1000) {
            jComboBox1.remove(1000);
        }
        jComboBox1.setSelectedIndex(0);
    }

    void log(String prefix, String str) {
        System.out.print(prefix + str);

        jComboBox1.setEditable(TRUE);

        sb.append(str);
        if (sb.length() > 20000) {
            sb.delete(0, sb.length() - 20000);
        }
        String strx = sb.toString();
        jTextArea1.setText(strx);
        if (!strx.endsWith("\r\n")) {
            jTextArea1.append("\r\n");
        }
        jTextArea1.invalidate();
        jTextArea1.setCaretPosition(sb.length());

    }

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        portTx(jTextField1.getText());

    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        int len = port.bytesAvailable();

        if (len > 0) {
            byte b[] = new byte[len + 1];
            int r = port.readBytes(b, len);
            b[r] = 0;
            String str = new String(b);
            log("Fetched:", str);
        }
    }

    void makeListWord() {
        portTx(": (list) ( -- ) cr dictionarystart begin dup 6 + ctype cr  dictionarynext until drop .\" .ok\n\"" + txTerm);
    }

    void loadWords() {
        try {
            portTx("(list)" + txTerm);

            int len = inputStream.available();
            int count = 0;

            while ((len <= 0) && (count++ < 50)) {
                Thread.sleep(50);
                len = inputStream.available();
            }
            if (count == 20) {
                log("LoadWords did not return in 2.5 seconds", "");
                return;
            }
            System.out.println("Rx: (count)" + len);
            // allwordsModel.removeAllElements();
            allwordsTree.removeAllChildren();
            JTree.DynamicUtilTreeNode currentNode = allwordsTree;

            JTree.DynamicUtilTreeNode hiddenNode = new JTree.DynamicUtilTreeNode("hidden", null);;
            hiddenNode.setAllowsChildren(true);

            JTree.DynamicUtilTreeNode printNode = new JTree.DynamicUtilTreeNode("Print", null);;
            printNode.setAllowsChildren(true);

            currentNode.add(printNode);
            currentNode.add(hiddenNode);

            while (len > 0) {
                byte b[] = new byte[len + 1];
                int r = port.readBytes(b, len);
                b[r] = 0;
                String str = new String(b);
                System.out.println("Rxed:"+str);
               // int ix = str.indexOf("---");
               // if (ix > 0) {
               //     str = str.substring(ix);
               // }
                String strs[] = str.split("\n");
                //log("LoadWords:", str);
//                DefaultListModel<String> allwordsModel = (DefaultListModel) jList1.getModel();
                //allwordsTree.createChildren(allwordsTree, strs);

                for (String s : strs) {
                    s = s.trim();
                    System.out.println(s);
                    if (!s.trim().equals("ok.")) {
                        // allwordsModel.addElement(s);
                        JTree.DynamicUtilTreeNode node = new JTree.DynamicUtilTreeNode(s, null);
                        if (s.startsWith("---")) {
                            currentNode = node;
                            currentNode.setAllowsChildren(true);
                            int iz = allwordsTree.getChildCount() - 1;
                            allwordsTree.insert(currentNode, iz);
                        } else if (s.startsWith("(")) {
                            hiddenNode.add(node);
                        } else if ((s.startsWith(".")) || (s.endsWith("."))) {
                            printNode.add(node);
                        } else {
                            boolean present = false;
                            Enumeration en = currentNode.children();
                            while (en.hasMoreElements()) {
                                JTree.DynamicUtilTreeNode nx = (JTree.DynamicUtilTreeNode) en.nextElement();
                                if (nx.getUserObject().toString().equals(node.getUserObject().toString())) {
                                    present = true;
                                }
                            }
                            if (!present) {
                                currentNode.add(node);
                            }
                        }
                    }

                }
                len = inputStream.available();
            }
        } catch (Exception exc) {
            log("loadWords", exc.getLocalizedMessage());
            exc.printStackTrace();
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    void loadWordsFromStream() {
        try {
            portTx("(list)" + txTerm);
            InputStreamReader rdr = new InputStreamReader(inputStream);

            BufferedReader br = new BufferedReader(rdr);
            int len = inputStream.available();
            int count = 0;

            while ((len <= 0) && (count++ < 50) &&(!br.ready())) {
                Thread.sleep(50);
                len = inputStream.available();
            }
            if (count == 100) {
                log("LoadWords did not return in 2.5 seconds", "");
                return;
            }
            System.out.println("Rx: (count)" + len);
            // allwordsModel.removeAllElements();
            allwordsTree.removeAllChildren();
            JTree.DynamicUtilTreeNode currentNode = allwordsTree;

            JTree.DynamicUtilTreeNode hiddenNode = new JTree.DynamicUtilTreeNode("hidden", null);;
            hiddenNode.setAllowsChildren(true);

            JTree.DynamicUtilTreeNode printNode = new JTree.DynamicUtilTreeNode("Print", null);;
            printNode.setAllowsChildren(true);

            currentNode.add(printNode);
            currentNode.add(hiddenNode);

            String s = br.readLine();
            s = s.trim();
            while (!"ok.".equals(s)) {
                s = s.trim();
                System.out.println(s);
                // allwordsModel.addElement(s);
                JTree.DynamicUtilTreeNode node = new JTree.DynamicUtilTreeNode(s, null);
                if (s.startsWith("---")) {
                    currentNode = node;
                    currentNode.setAllowsChildren(true);
                    int iz = allwordsTree.getChildCount() - 1;
                    allwordsTree.insert(currentNode, iz);
                } else if (s.startsWith("(")) {
                    hiddenNode.add(node);
                } else if ((s.startsWith(".")) || (s.endsWith("."))) {
                    printNode.add(node);
                } else {
                    boolean present = false;
                    Enumeration en = currentNode.children();
                    while (en.hasMoreElements()) {
                        JTree.DynamicUtilTreeNode nx = (JTree.DynamicUtilTreeNode) en.nextElement();
                        if (nx.getUserObject().toString().equals(node.getUserObject().toString())) {
                            present = true;
                        }
                    }
                    if (!present) {
                        currentNode.add(node);
                    }
                }
                count = 0;
                while ((count++ < 20) && (inputStream.available() < 1) && (!br.ready())) {
                    Thread.sleep(50);
                }
                if (count >= 20) {
                    log("LoadWOrdsFromStream", "Timeout");
                    return;
                }
                while(!br.ready()) 
                    Thread.sleep(100);
                s = br.readLine();
                
                if (s.startsWith("ok."))
                    count=21;
                s.trim();
                System.out.println("Received:" + s);
            }
        } catch (Exception exc) {
            log("loadWords", exc.getLocalizedMessage());
            exc.printStackTrace();
        }
    }

    private void openMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openMenuItemActionPerformed
        FileLoader dialog = new FileLoader(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
    }//GEN-LAST:event_openMenuItemActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        loadWords(); 
        // loadWordsFromStream();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
        txRx(jTextField1.getText());
        jTextField1.setText("");

    }//GEN-LAST:event_jTextField1ActionPerformed

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        if (jComboBox1.getSelectedItem() != null) {
            txRx(jComboBox1.getSelectedItem().toString());
        } else {
            log("jComboBox1ActionPerformed", "Nothing selected in history");
        }
    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        makeListWord();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    boolean loadFile(String filename) {
        int lineNum = 0;
        boolean loading = true;
        try {
            log("LoadFile:", filename);
            txRx(" : ---" + filename + "--- ;");
            File f = new File(filename);
            //InputStream is =new FileInputStream(f);
            BufferedReader br = new BufferedReader(new FileReader(f));
            String str = br.readLine();
            lineNum++;

            while (loading && (str != null)) {
                String strs[] = str.split(" ");
                for (int i = 0; i < strs.length; i++) {
                    String s = strs[i].toLowerCase();
                    if (s.equals("#include")) {
                        loading = loadFile(strs[i + 1]);
                    } else if (s.equals("#require")) {
                        loading = loadFile(strs[i + 1]);
                    } else if (s.equals("(")) {
                        while (i < strs.length && (!strs[i++].equals(")")));
                    } else if (s.equals("\\")) {
                        i = strs.length;
                    } else {
                        outputStream.write(' ');
                        outputStream.write(s.getBytes());
                    }

                    outputStream.flush();
                    Thread.sleep(50);
                    String ret = portRx();
                    if (!ret.startsWith("Exception")) {
                        loading = false;
                        i = strs.length;
                        log("loadFile: ", String.format(" %s problem loading line %i [%s]: %s", filename, lineNum, s, str));
                    }
                }
                str = br.readLine();
                lineNum++;
            }

        } catch (Exception exc) {
            log("File Load:", exc.getLocalizedMessage());
        }
        return loading;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ForthIDEMain.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ForthIDEMain.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ForthIDEMain.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ForthIDEMain.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                fm = new ForthIDEMain();
                fm.setVisible(true);
            }
        });
        // fm.checkPort();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem aboutMenuItem;
    private javax.swing.JMenuItem contentMenuItem;
    private javax.swing.JMenuItem copyMenuItem;
    private javax.swing.JMenuItem cutMenuItem;
    private javax.swing.JMenuItem deleteMenuItem;
    private javax.swing.JDesktopPane desktopPane;
    private javax.swing.JMenu editMenu;
    private javax.swing.JMenuItem exitMenuItem;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JMenu helpMenu;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTree jTree1;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem openMenuItem;
    private javax.swing.JMenuItem pasteMenuItem;
    private javax.swing.JMenuItem saveAsMenuItem;
    private javax.swing.JMenuItem saveMenuItem;
    // End of variables declaration//GEN-END:variables

}
