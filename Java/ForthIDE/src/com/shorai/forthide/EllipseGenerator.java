/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shorai.forthide;

import java.io.*;
import java.util.Locale;

/**
 *
 * @author chris

For GErhard Fourie's cover plates we do the following 

First set leveling cut to 9mm
ECHO: "Origin at  X:", 84.5, "  Y:", 63.5
oRIng Groove      ECHO: "Elliptic Cut dx:", 145, " Dy:", 105, "  Height:", 12, "  Tool Diam:", 5 
Inner retaining   ECHO: "Elliptic Cut dx:", 167.2, " Dy:", 125.2, "  Height:", 12, "  Tool Diam:", 5
Undercut          ECHO: "Elliptic Cut dx:", 165.6, " Dy:", 123.6, "  Height:", 4, "  Tool Diam:", 9.5
Edge              ECHO: "Elliptic Cut dx:", 172, " Dy:", 130, "  Height:", 10, "  Tool Diam:", 5
* 
* #; Usage: -classpath build/classes com.shorai.forthide.ElliptiseGenerator X Y dx dy startz endz zPerRev stepsPerRev safeZ feedRate filename
* #       X   Y  dx    dy    startz endz zPerRev stepsPerRev safeZ feedRate
* cd ~/git/forthide/Java/ForthIDE
* 
java -classpath build/classes com.shorai.forthide.ElipseGenerator 90.0 70.0 145.0 105.0   2.0  -7.5  -1.0      360.0       20.0    600.0  oring_1.gcode
java -classpath build/classes com.shorai.forthide.ElipseGenerator 90.0 70.0 166.0 125.0   2.0  -7.5  -1.0      360.0       20.0    600.0  fitted_2.gcode
java -classpath build/classes com.shorai.forthide.ElipseGenerator 90.0 70.0 166.0 125.0  -3.5  -7.5  -1.0      360.0       20.0    600.0  recess_3.gcode
java -classpath build/classes com.shorai.forthide.ElipseGenerator 90.0 70.0 172.0 130.0   0.0  -10.0 -1.0      360.0       20.0    600.0  outer_4.gcode

* 
* @TODO: Choose Inner or outer cut and get direction
* @TODO: Set tool diameter, the radius then becomes tthe finished size (inside or outside)
* @TODO: Set milling width independent of tool diameter ... calculates mulltiple passes
* @TODO: leave a number of TABS to hold workpiece on final cut
* @TODO: set material thickness
* @TODO: named parameters
* @TODO: GUI
* @TODO: Entry code for undercuts
* @TODO: multi cut at different radii
* 
*/

public class EllipseGenerator {
    
    
    static public void main(String args[]) { 
        String name[] = {"oring_1.gcode","fitted_2.gcode","recess_3.gcode","outer_4.gcode"}; 
        Double aa[][] = 
        { 
          // this cuts the inner oRing
            { 90.0, 70.0, 150.0, 150.00, 110.0, 110.00,  2.0,  -7.5,  -1.0,      360.0,    1.0,   20.0,    600.0, 0.00},
            // this cuts the outside cylinder clear for 9.5mm cutter down to the rim
            { 90.0, 70.0, 179.5, 168.5, 138.0,  127.5,   2.0,  -7.5,  -1.0,      360.0,    1.0,   20.0,    600.0, 0.00},
            // this cuts the outside grrove for outer oring seal    
            { 90.0, 70.0, 175.0, 169.0, 132.0,  128.0,  -3.5,  -7.5,  -1.0,      360.0,    1.0,   20.0,    600.0, 0.00},  
            // finally the rim cut complete with holding tabs
            { 90.0, 70.0, 172.0, 172.0, 130.0,  130.0,  -5.0,  -10.5, -1.0,      360.0,    1.0,   20.0,    600.0, 1.00},  // 5mm cutter
        };
        if ((args.length == 0) || "-h".equals(args[0])|| "--help".equals(args[0])|| "-?".equals(args[0])    ) {
                help();
                System.exit(0);
        }
        EllipseGenerator gen = new EllipseGenerator();
        try {
            if (args.length >= 12) { 
                for (int i=0; i < 11; i++) {
                    aa[0][i] = Double.parseDouble(args[i]);
                }
                gen.run(args[11],aa[0]);
            } else { 
                for (int i=0; i < name.length; i++) { 
                    gen.run(name[i],aa[i]);
                }
            } 
            } catch (Exception exc) { 
                System.out.println(exc.getLocalizedMessage());
                exc.printStackTrace();
            }
    } 
    
   public void run(String fileName, Double args[]) throws Exception { 
     
        File f = new File(fileName);
        //OutputStream os = new java.io.FileOutputStream(f);
       PrintWriter bw = new PrintWriter(new FileWriter(fileName));
        double x=0.0;
        double y = 0.0;
        double dx0 = 100.0;
        double dx1 = 100.0;
        double dy0 = 100.0;
        double dy1 = 100.0;
        double startz = 0.0;
        double endz = -2.0;
        double zPerRev = 1.0;
        double safeZ = 10.0;
        int stepsPerRev = 64;
        double feedRate = 50;
        double tabs = 0.0;
/*
        System.out.printf("; argc:%d\n",args.length);
        for (int i=0; i < args.length; i++) {
                System.out.printf("; Generate Ellipse Arg %d : %s\n", i , args[i]);
        }
*/
    
        int i = 0;
        try { 
        x =             args[i++];
        y =             args[i++];
        dx0 =            args[i++];
        dx1 =            args[i++];
        dy0 =            args[i++];
        dy1 =            args[i++];
        startz =        args[i++];
        endz =          args[i++];
        zPerRev =       args[i++];
        stepsPerRev =   args[i++].intValue();
        double cutDepth       = args[i++];
        safeZ =         args[i++];
        feedRate        = args[i++];
        tabs            = args[i++];
        
        int circuits = Math.abs((int) ((startz-endz)/zPerRev));

        // correct steps to be approx PI per mm  
        stepsPerRev = Math.max(stepsPerRev, (int) Math.max(dx0,dy0) * 10);
        
        bw.printf("\n;\tParameters (X:%.3f,Y:%.3f) [dx:%.3f dy:%.3f] to [dx:%.3f dy:%.3f]\n;\tstartz:%.3f endZ:%.3f zPerRev:%.3f \n;\tCutDepth:%8.3f\n;\tsafeZ:%.3f \n;\tcircuits:%d \n;\tStepsPerRev:%d \n;\tFeedRate:%8.3f\n;\tTabs:%f\n",
        x,y,dx0, dy0,dx1, dy1, startz, endz, zPerRev, cutDepth, safeZ,circuits, stepsPerRev,feedRate,tabs);

        bw.printf(Locale.ROOT,"S255\nG0 Z%.3f\nG0 X1Y1\nG1 X0 Y0 F%f\nm3m8\n",safeZ,feedRate,tabs);

        
        
        //ellipticCylinder(bw,x,y,dx0,dy0,startz,endz,zPerRev, circuits,stepsPerRev,tabs);
        ellipticMill(bw,x,y,dx0,dx1,dy0,dy1,startz,endz,zPerRev, cutDepth,circuits,stepsPerRev, tabs);
        
        bw.printf(Locale.ROOT,"\n\nG0 Z%.3f\nM5M9\nG0 X0 Y0\n",safeZ);
        } catch (Exception exc) { 
            System.out.printf("Exception : %s", exc.getLocalizedMessage());
            exc.printStackTrace();
            
        }
        bw.flush();
        bw.close();
    }
 
static void help() {
        System.out.printf("Usage: ellipticCutter X Y dx0 dy0 dx1 dy1 startz endz zPerRev stepsPerRev safeZ feedRate tabs filename\n");
        System.out.printf("All numerics are double and require a decimal point\n");
        System.out.printf("If no parameters are given, you get 4 precompiled files for my test job - Could and should be a test class\n");
        System.out.println("Send some parameters, < 11 to run internal tests");

}

/**
 * This method cuts an elliptic cylinder with fixed radii and heights
 * It proceeds as follows
 *       - Cut an ellipse at the start height
 *       - Cut a helix down to the endZ
 *       - finish with a circuit at the end height
 * 
 * @param pw  - PrintWriter to receive output
 * @param x   - X centre of the ellipse
 * @param y     - y center of the ellipse
 * @param dx    - diameter in X
 * @param dy    - diameter in Y
 * @param startz    - starting cutter height
 * @param endz      - ending cutter height
 * @param zPerRev   - z depth of cut
 * @param circuits  - number of circuits in helix
 * @param stepsPerRev - number of segments in a circuit
 * @param tabs        - depth of holding tabs left at end to hold work, 0.0 or negative means no tabs
 */

void ellipticCylinder(PrintWriter pw, double x, double y, double dx, double dy, double startz, double endz, double zPerRev, int circuits, int stepsPerRev, double tabs) {
        double dz = startz-endz;
        pw.printf(Locale.ROOT,"G0 X%.3f Y%.3f\nG1 Z%.3f\n", x, y+dy/2.0, startz);
        
        pw.printf(";  initial top cut circuit\n");
        for (int j=0; j < stepsPerRev; j++) {
                        double theta = -j * 2.0 * Math.PI / stepsPerRev;
                        double xx = dx * Math.sin(theta)/2.0;
                        double yy = dy * Math.cos(theta)/2.0;
                        double z = startz;
                        pw.printf(Locale.ROOT,"G01 X%.3f Y%.3f Z%.3f\n", x+xx, y+yy, z);
                }
        
        pw.printf(";  helical downcut\n");
        for (int i = 0; i < circuits; i++) {
                for (int j=0; j <= stepsPerRev; j++) {
                        double theta = -j * 2.0 * Math.PI / stepsPerRev;
                        double xx = dx * Math.sin(theta)/2.0;
                        double yy = dy * Math.cos(theta)/2.0;
                        double z = startz + zPerRev * (double) (i*stepsPerRev + j)/(double) stepsPerRev;
                        z = Math.max(z,endz);
                        // holding tabs 
                        if ((tabs>0.1) && (z < endz+tabs) && ((Math.abs(xx) < 5.0) || (Math.abs(yy) < 5.0))) {
                            z = endz+0.5;
                        }
                        pw.printf(Locale.ROOT,"G01 X%.3f Y%.3f Z%.3f\n", x+xx, y+yy, z);
                }
        }
        pw.printf(";  final clean up round\n");

                for (int j=0; j < stepsPerRev; j++) {
                        double theta = -j * 2.0 * Math.PI / stepsPerRev;
                        double xx = dx * Math.sin(theta)/2.0;
                        double yy = dy * Math.cos(theta)/2.0;
                        double z = endz;
                        if ((tabs > 0.1) && (z < endz+tabs) && ((Math.abs(xx) < 5.0) || (Math.abs(yy) < 5.0))) {
                            z = endz+0.5;
                        }
                        pw.printf(Locale.ROOT,"G01 X%.3f Y%.3f Z%.3f\n", x+xx, y+yy, z);
                }
        pw.printf(";  return to startXYZ where Z is clear above us to retract tool\n");

        pw.printf(Locale.ROOT,"G0 X%.3f Y%.3f\nG1 Z%.3f\n", x, y+dy/2.0, startz);

}
/**
 * Perform an ellipticCylinder at different radii between start and end diameters 
 * 
 * @param pw
 * @param x
 * @param y
 * @param dx0, dx1    start and end x diameters
 * @param dy0, dx1    start and end y diameters
 * @param startz
 * @param endz
 * @param zPerRev
 * @param cutDepth   - the depth of cut we take on each pass
 * @param circuits
 * @param stepsPerRev
 * @param tabs
 * The method first cuts at the start radii for the full height of the cut
 * Then moves cutdepth towards the final radii and cuts again the full height
 * Progresses till both x and y diameters have reached their targets
 * @TODO: Does not check parameters to see tool shaft is OK, i.e. a cutter has a max tool depth
 * This method will cut either an inner or an outer circumference depending on diameters supplied
 * It is also allowable to have diameter X and diameter Y going in opposite directions
 */  
void ellipticMill(PrintWriter pw, double x, double y, double dx0,double dx1, double dy0, double dy1,
        double startz, double endz, double zPerRev, double cutDepth, int circuits, int stepsPerRev, double tabs) {
    
    double diamX = dx0;
    double diamY = dy0;
    
    int cutNumber = 0;
    boolean xFinalCut = false;
    boolean yFinalCut = false;
    
    while (!(xFinalCut & yFinalCut)) {
         pw.printf("\n;\tEllipticCylinder(X:%.3f,Y:%.3f) [dx:%.3f dy:%.3f] \n;\tstartz:%.3f endZ:%.3f zPerRev:%.3f \n;\tCutDepth:%8.3f\n;\tcircuits:%d \n;\tStepsPerRev:%d \n;\tTabs:%f\n",
        x,y,diamX, diamY, startz, endz, zPerRev, cutDepth, circuits, stepsPerRev,tabs);

       ellipticCylinder(pw, x, y, diamX, diamY, startz, endz, zPerRev, circuits, stepsPerRev, tabs);
        if (dx1>dx0) {
            diamX += cutDepth;
            if (diamX > dx1) {
                diamX = dx1;
                xFinalCut = true;
            }
        } else { 
            diamX-= cutDepth;
            if (diamX < dx1) {
                diamX = dx1;
                xFinalCut = true;
            }
        
        if (dy1>dy0) {
            diamY += cutDepth;
            if (diamY > dy1) {
                diamY = dy1;
                yFinalCut = true;
            }
        } else { 
            diamY-= cutDepth;
            if (diamY < dy1) {
                diamY = dy1;
                yFinalCut = true;
            }
        }
    }
}
}
}

