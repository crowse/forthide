/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shorai.forthide;

import javax.swing.JTree;

/**
 *  Node to represent an abstract node in our FORTH word hierarchy
 * 
 * The basic extension provides a mechanism to provide the programmer with
 * - Source filename, linenumber of definition
 * - Additional help text for core words
 * - mouse over information
 * 
 * @TODO: provide a local database that contains definition, mouseover and help text
 * @TODO: provide extended comment tags e.g. {m ...} {h .....} INSIDE definitions
 * 
 * @TODO: As we tokenise and load definitions to the FORTH cpu, we want to glean
 * as much info as possible to assist in the future reuse of the definition
 * 
 * 
 * @TODO: either identify or FLAG constants and Variables so we can display them separately
 * @TODO: query and display variables and stack whenever the terminal is idle or on user request (.s @v)
 * 
 * @author Chris
 */
public class AbstractWordNode extends JTree.DynamicUtilTreeNode {
     String mouseOverText;
     String sourceFile;
     String fileNumber;
  
    public AbstractWordNode(String name, Object children) {
        super(name,children);
    }
    
}
