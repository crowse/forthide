\ forgetram

\ @TODO:  Sense the gap multiple times and get time of top to pass gap
\ @TODO:  More musical sounds
\ @TODO:  put 3 sensors in series
\ @TODO:  run must go on forever
\ @TODO:  watchdog restart
\ @TODO:  maintain counts over power failure (VBat?)
\ @TODO:  Cost in a rechargeable lipo
\ @TODO:  Increase volume by pumping the voltage
\ bottles

\ compiletoflash
\ cornerstone bottles 

2   6 io constant buzzer 
2   7 io constant red0
2   8 io constant red1
2   9 io constant red2
0   8 io constant red3

0 12 io constant gapPin

: initPins 
    OMODE-PP buzzer io-mode! 
    omode-PP red0 io-mode!
    omode-PP red1 io-mode!
    omode-PP red2 io-mode!
    omode-PP red3 io-mode!

    imode-float gapPin io-mode!
;

300 constant d0
260 constant d1
190 constant d2
160 constant d3

3 constant payoutFrequency 

0 variable counter
0 variable payouts

: delay   0 do loop ;

: buzz 4000 0 do 
    buzzer iox!  
       i d0 mod 0= if red0 iox! then 
       i d1 mod 0= if red1 iox! then 
       i d2 mod 0= if red2 iox! then 
       i d3 mod 0= if red3 iox! then
       400 delay 
    loop 
    buzzer ioc! 
    red0 ioc! red1 ioc! red2 ioc! red3 ioc! ;

: pwd   " atcdGip$" ;
0 variable pcnt 
 
\ checks input against password
: passwd? key? if pwd swap pcnt @ key = 
                   if 1 pcnt +! pcnt = if true else false then
              else drop false 
              then ; 


: run initPins  begin
       gapPin io@  
       if   1 counter +! 
            counter @ payoutFrequency mod 0= 
              if buzz 1 payouts +! 10000 delay then
        begin gapPin io@ 0= until
        ." Counter: " counter @ . ." payouts:" payouts @ . cr        
        then
       key? until key drop
       ;

: init run ; 
compiletoram

