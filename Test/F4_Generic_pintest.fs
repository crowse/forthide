\ stm32F4 pin tester


\ Test strategy
\   - set all pins to input (PA-PE pins 0-15) less the ones that are predefined forr he board
\     Initial scan should be using SWDIO only and a boundary scan
\         predefined == USB, Serial, fitted peripherals etc

\   - for each pin apply float, pullup, pulldown and check that it is the only one asserted
\   - 
\ IGNORE pins
6 variable F4DiscoPredefined  0 13 io , 0 14 io , 				\ SWDIO 
                              0  9 io , 0 10 io , 0 11 , 0 12 , \ USART and USB
							  7  0 io , 7  1 io ,  				\ PH0 and PH1 Oscillator 
							  
0 variable PA   24 allot
PA  4 + constant PB
PB  4 + constant PC
PC  4 + constant PD
PD  4 + constant PE
PE  4 + constant PH


: .ports  7 0 do  
         [char] P emit I [char] A + I 5 > if 3 + then emit 
		 i 4 * PA + @
		 0 <# # # # # 20 hold # # # # 20 hold # # # # 20 hold # # # # #> .  space 
	loop ;  

: readAllPins 5 0 do 
	I dup io-port [char] A + emit 
	          16 + @  I 4 * PA + ! 
    loop 
	7 io-port @ PH ! 
	; 

: modeAllPins ( mode --) 
    5 0 do I
		16 0 do 
			2dup I io io-mode! 
		loop
	loop
	dup 7 0 io io-mode!
	    7 1 io io-mode!
	; 

\ Test everything first with all set to FLOAT
: testPinsInput ( mode--)  modeAllPins readAllPins IMODE-FLOAT modeAllPins ." IMODE-FLOAT:" .ports ;

\ Test a pin driven in an input mode 
\  First test float mode, then all pulled up, then all pulled down 
: testPinIMode ( pin, mode --)  IMODE-FLOAT modeAllPins 
		over io-mode! readAllPins IMODE-FLOAT swap io-mode!
		IMODE-FLOAT modeAllPins ." IMODE-FLOAT:" .ports ;
: testPinsInput ( mode --)
     5 0 do I
		16 0 do i io dup >R over io-mode!
             readAllPins 	
             IMODE-FLOAT R> io-mode!
             .ports
		loop
	loop
	;

: testPinsPuPd ( mode --) dup modeAllPins
     5 0 do I
		16 0 do i io 
		     IMODE-PU over io-mode! dup io@ >R
             IMODE-PD over io-mode! dup io@ dup R>
             = if 0= if [char] 0 else [char] 1 then else [char] . then emit			 
			 over swap io-mode!   
			 i 4 mode 0= if space then 
		loop space space space 
	loop
	;



\ Now we can start real tests - successively set all other pins to float, PU and PD
\     now drive selected pin in mode to high and low, checking each time  
: testPinOMode ( pin, mode, other pins mode --)  modeAllPins 
		over io-mode! dup ioc! readAllPins ." Set IO to 0:" .ports
		              dup ios! readAllPins ." Set IO to 1:" .ports
		IMODE-FLOAT swap io-mode!
		IMODE-FLOAT modeAllPins ." IMODE-FLOAT:" .ports ;

\ ********************************************************** actual tests
\ in the basic tests we check that the weak pull up / pull downs can toggle the pins
: basicTests  CR ." IMODE-FLOAT Results should be random or allZero" cr 
	." Test with pins set float" cr IMODE-FLOAT testPinsInput   \ basic test shows which pins are tied high
	  CR ." IMODE-HIGH Results should be all High" cr 
	." Test with pins set PU"    cr IMODE-HIGH  testPinsInput   \ basic test shows which pins are tied high
	  CR ." IMODE-LOWResults should be allZero" cr 
	." Test with pins set PD"    cr IMODE-LOW   testPinsInput   \ basic test shows which pins are tied high
	;
	
\ In intermediate tests we check that weak outputs only toggle the pin they are applied to 
\    we do this bu putting weak PU and PD resistors on other pins, then put opposite pull on 1 pin
\    FLOAT mode is not useful as it should give random results
  
: intermediateTests
	." Test with pins weak PU" cr  IMODE-HIGH testPinsPuPd
	." Test with pins weak PD" cr  IMODE-LOW  testPinsPuPd
;
\ *********************************************************** exhaustive tests below AFTER above have passed
: exhaustiveTests
OMODE-WEAK OMODE-OD + 0 0 IMODE-FLOAT testPinOMode
OMODE-WEAK OMODE-OD + 0 0 IMODE-HIGH  testPinOMode
OMODE-WEAK OMODE-OD + 0 0 IMODE-LOW   testPinOMode

OMODE-SLOW OMODE-OD + 0 0 IMODE-FLOAT testPinOMode
OMODE-SLOW OMODE-OD + 0 0 IMODE-HIGH  testPinOMode
OMODE-SLOW OMODE-OD + 0 0 IMODE-LOW   testPinOMode

           OMODE-OD   0 0 IMODE-FLOAT testPinOMode
           OMODE-OD   0 0 IMODE-HIGH  testPinOMode
           OMODE-OD   0 0 IMODE-LOW   testPinOMode

OMODE-FAST OMODE-OD + 0 0 IMODE-FLOAT testPinOMode
OMODE-FAST OMODE-OD + 0 0 IMODE-HIGH  testPinOMode
OMODE-FAST OMODE-OD + 0 0 IMODE-LOW   testPinOMode

OMODE-WEAK OMODE-OD-HIGH + 0 0 IMODE-FLOAT testPinMode

OMODE-WEAK OMODE-PP + 0 0 IMODE-FLOAT testPinMode
;
( OMODE-NORMAL = 0) OMODE-FAST ; ( values -2, -1, 0, 1 )