@
@    Mecrisp-Stellaris - A native code Forth implementation for ARM-Cortex M microcontrollers
@    Copyright (C) 2013  Matthias Koch
@
@    This program is free software: you can redistribute it and/or modify
@    it under the terms of the GNU General Public License as published by
@    the Free Software Foundation, either version 3 of the License, or
@    (at your option) any later version.
@
@    This program is distributed in the hope that it will be useful,
@    but WITHOUT ANY WARRANTY; without even the implied warranty of
@    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
@    GNU General Public License for more details.
@
@    You should have received a copy of the GNU General Public License
@    along with this program.  If not, see <http://www.gnu.org/licenses/>.
@

@ -----------------------------------------------------------------------------
@ Interruptvektortabelle
@ -----------------------------------------------------------------------------

.include "../common/vectors-common.s"

@ Special interrupt handlers for this particular chip:

.word nullhandler+1 @ Position  0: Window Watchdog
.word nullhandler+1 @ Position  1: PVD through EXTI line detection
.word nullhandler+1 @ Position  2: Tamper and TimeStamp through EXTI line
.word nullhandler+1 @ Position  3: RTC Wakeup
.word nullhandler+1 @ Position  4: Flash
.word nullhandler+1 @ Position  5: RCC
.word irq_vektor_exti0+1 @ Position  6: EXTI Line 0
.word irq_vektor_exti1+1 @ Position  7: EXTI Line 1
.word irq_vektor_exti2+1 @ Position  8: EXTI Line 2
.word irq_vektor_exti3+1 @ Position  9: EXTI Line 3
.word irq_vektor_exti4+1 @ Position 10: EXTI Line 4
.word irq_vektor_dma1_0+1 @ Position 11: DMA1 Stream 0
.word irq_vektor_dma1_1+1 @ Position 12: DMA1 Stream 1
.word irq_vektor_dma1_2+1 @ Position 13: DMA1 Stream 2
.word irq_vektor_dma1_3+1 @ Position 14: DMA1 Stream 3
.word irq_vektor_dma1_4+1 @ Position 15: DMA1 Stream 4
.word irq_vektor_dma1_5+1 @ Position 16: DMA1 Stream 5
.word irq_vektor_dma1_6+1 @ Position 17: DMA1 Stream 6
.word irq_vektor_adc+1 @ Position 18: ADC global interrupts
.word irq_vektor_can_tx+1 @ Position 19:
.word irq_vektor_can1_rx0+1 @ Position 20:
.word irq_vektor_can1_rx1+1 @ Position 21:
.word irq_vektor_can1_sce+1 @ Position 22:
.word irq_vektor_exti5+1 @ Position 23:
.word irq_vektor_tim1_brk+1 @ Position 24:
.word irq_vektor_tim1_up+1 @ Position 25:
.word irq_vektor_tim1_trg+1 @ Position 26:
.word irq_vektor_tim1_cc+1 @ Position 27:
.word irq_vektor_tim2+1 @ Position 28: Timer 2 global interrupt
.word irq_vektor_tim3+1 @ Position 29: Timer 3 global interrupt
.word irq_vektor_tim4+1 @ Position 30: Timer 4 global interrupt

.word irq_vektor_i2c1_ev+1 @ Position 31:
.word irq_vektor_i2c1_er+1 @ Position 32:
.word irq_vektor_i2c2_ev+1 @ Position 33:
.word irq_vektor_i2c_er+1 @ Position 34:
.word irq_vektor_spi1+1 @ Position 35:
.word irq_vektor_spi2+1 @ Position 36:
.word irq_vektor_usart1+1 @ Position 37:
.word irq_vektor_usart2+1 @ Position 38:
.word irq_vektor_usart3+1 @ Position 39:

.word irq_vektor_exti10+1 @ Position 40:
.word irq_vektor_rtc+1 @ Position 41:
.word irq_vektor_otg_fs_wkup+1 @ Position 42:
.word irq_vektor_tim8_brk+1 @ Position 43:
.word irq_vektor_tim8_up+1 @ Position 44:
.word irq_vektor_tim8_trg+1 @ Position 45:
.word irq_vektor_tim8_cc+1 @ Position 46:
.word irq_vektor_dma1_7+1 @ Position 47:
.word irq_vektor_fsmc+1 @ Position 48:
.word irq_vektor_sdio+1 @ Position 49:

.word irq_vektor_tim5+1 @ Position 50:
.word irq_vektor_spi3+1 @ Position 51:
.word irq_vektor_uart4+1 @ Position 52:
.word irq_vektor_uart5+1 @ Position 53:
.word irq_vektor_tim6_dac+1 @ Position 54:
.word irq_vektor_tim7+1 @ Position 55:
.word irq_vektor_dma2_0+1 @ Position 56:
.word irq_vektor_dma2_1+1 @ Position 57:
.word irq_vektor_dma2_2+1 @ Position 58:
.word irq_vektor_dma2_3+1 @ Position 59:

.word irq_vektor_dma2_4+1 @ Position 60:
.word irq_vektor_eth+1 @ Position 61:
.word irq_vektor_eth_wkup+1 @ Position 62:
.word irq_vektor_can2_tx+1 @ Position 63:
.word irq_vektor_can2_rx0+1 @ Position 64:
.word irq_vektor_can2_rx1+1 @ Position 65:
.word irq_vektor_can2_sce+1 @ Position 66:
.word irq_vektor_otg_fs+1 @ Position 67:
.word irq_vektor_dma2_5+1 @ Position 68:
.word irq_vektor_dma2_6+1 @ Position 69:

.word irq_vektor_dma2_7+1 @ Position 70:
.word irq_vektor_usart6+1 @ Position 71:
.word irq_vektor_i2c3_ev+1 @ Position 72:
.word irq_vektor_i2c3_er+1 @ Position 73:
.word irq_vektor_otg_hs_ep1_out+1 @ Position 74:
.word irq_vektor_otg_hs_ep1_in+1 @ Position 75:
.word irq_vektor_otg_hs_wkup+1 @ Position 76:
.word irq_vektor_otg_hs+1 @ Position 77:
.word irq_vektor_dcmi+1 @ Position 78:
.word irq_vektor_cryp+1 @ Position 79:

.word irq_vektor_hash_rng+1 @ Position 80:
.word irq_vektor_fpu+1 @ Position 81:

@ -----------------------------------------------------------------------------
