@
@    Mecrisp-Stellaris - A native code Forth implementation for ARM-Cortex M microcontrollers
@    Copyright (C) 2013  Matthias Koch
@
@    This program is free software: you can redistribute it and/or modify
@    it under the terms of the GNU General Public License as published by
@    the Free Software Foundation, either version 3 of the License, or
@    (at your option) any later version.
@
@    This program is distributed in the hope that it will be useful,
@    but WITHOUT ANY WARRANTY; without even the implied warranty of
@    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
@    GNU General Public License for more details.
@
@    You should have received a copy of the GNU General Public License
@    along with this program.  If not, see <http://www.gnu.org/licenses/>.
@

@ Routinen für die Interrupthandler, die zur Laufzeit neu gesetzt werden können.
@ Code for interrupt handlers that are exchangeable on the fly

@------------------------------------------------------------------------------
@ Alle Interrupthandler funktionieren gleich und werden komfortabel mit einem Makro erzeugt:
@ All interrupt handlers work the same way and are generated with a macro:
@------------------------------------------------------------------------------

interrupt exti0
interrupt exti1
interrupt exti2
interrupt exti3
interrupt exti4

interrupt dma1_0
interrupt dma1_1
interrupt dma1_2
interrupt dma1_3
interrupt dma1_4
interrupt dma1_5
interrupt dma1_6

interrupt adc
interrupt can1_tx
interrupt can1_rx0
interrupt can1_rx1
interrupt can1_sce
interrupt exti5

interrupt tim1_brk
interrupt tim1_up
interrupt tim_trg
interrupt tim1_cc

interrupt tim2
interrupt tim3
interrupt tim4

interrupt i2c1_ev
interrupt i2c1_er
interrupt i2c2_ev
interrupt i2c2_er
interrupt spi1
interrupt spi2

interrupt usart1
interrupt usart2
interrupt usart3
interrupt exti10

interrupt rtc_alarm
interrupt otg_fs_wkup
interrupt tim8_brk
interrupt tim8_up
interrupt tim8_trg
interrupt tim8_cc


interrupt dma1_7


interrupt fsmc
interrupt sdio
interrupt tim5
interrupt spi3
interrupt uart4
interrupt uart5
interrupt tim6_dac
interrupt tim7


interrupt dma2_0
interrupt dma2_1
interrupt dma2_2
interrupt dma2_3
interrupt dma2_4

interrupt eth
interrupt eth_wkup
interrupt can2_tx
interrupt can2_rx0
interrupt can2_rx1
interrupt can2_sce
interrupt otg_fs


interrupt dma2_5
interrupt dma2_6
interrupt dma2_7

interrupt usart6
interrupt i2c3_ev
interrupt i2c3_er

interrupt otg_hs_ep1_out
interrupt otg_hs_ep1_in
interrupt otg_hs_wkup
interrupt otg_hs
interrupt dcmi
interrupt cryp
interrupt hash_rng
interrupt fpu






@------------------------------------------------------------------------------

