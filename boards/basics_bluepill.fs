\ Basic common environment for Cortex M3 Bluepill processors 
\ Machine independent basic definitions that give a consistent 
\ FORTH VM and environment
\ requires registers and basic IO to be preloaded
\ bluepill does not yet have Vocs because dot have -ra version burned in flash

\ At the end of loading this file for your CPU using e4th you should have a comfortable working environment
\ the environment uses flash up to about $C000 (48Kb) and 4 bytes of RAM (cpuHz)
\ Can we get rid of this by using a counter/constant in systick or RTC?

\ We should be able to duplicate this structure on any modern ARM CPU and still have space for the app

\ LedStatus ios!

compiletoflash

 : cornerstone ( Name ) ( -- )   
    <builds begin here $3FF and while 0 h, repeat
   does>   begin dup  $3FF and while 2+   repeat
          eraseflashfrom
  ;
8000000 variable cpuHz
72000000 constant maxCPUHz
5 constant numGPIOPorts

: sysname s" BluePill" ;

\ registers takes much too much space from the 64Kb (nearly 16Kb)
\ by putting these here we are at about 3.1Kb .... saving 12Kb
\ #require  ../Registers/STM32F103xx.fs

   $40010000 constant AFIO  
       AFIO $0 + constant AFIO_EVCR
       AFIO $4 + constant AFIO_MAPR
       AFIO $8 + constant AFIO_EXTICR1
       AFIO $C + constant AFIO_EXTICR2
       AFIO $10 + constant AFIO_EXTICR3
       AFIO $14 + constant AFIO_EXTICR4
       AFIO $1C + constant AFIO_MAPR2
   
 $40021000 constant RCC  
       RCC $0 + constant RCC_CR
       RCC $4 + constant RCC_CFGR
       RCC $8 + constant RCC_CIR
       RCC $C + constant RCC_APB2RSTR
       RCC $10 + constant RCC_APB1RSTR
       RCC $14 + constant RCC_AHBENR
       RCC $18 + constant RCC_APB2ENR
       RCC $1C + constant RCC_APB1ENR
       RCC $20 + constant RCC_BDCR
       RCC $24 + constant RCC_CSR

 $40022000 constant FLASH  
       FLASH $0 + constant FLASH_ACR
 \      FLASH $4 + constant FLASH_KEYR
 \      FLASH $8 + constant FLASH_OPTKEYR
 \      FLASH $C + constant FLASH_SR
 \      FLASH $10 + constant FLASH_CR
 \      FLASH $14 + constant FLASH_AR
 \      FLASH $1C + constant FLASH_OBR
 \      FLASH $20 + constant FLASH_WRPR

    $40013800 constant USART1  
       USART1 $0 + constant USART1_SR
       USART1 $4 + constant USART1_DR
       USART1 $8 + constant USART1_BRR
       USART1 $C + constant USART1_CR1
       USART1 $10 + constant USART1_CR2
       USART1 $14 + constant USART1_CR3
       USART1 $18 + constant USART1_GTPR
    
     $40012C00 constant TIM1  
       TIM1 $0 + constant TIM1_CR1
       TIM1 $4 + constant TIM1_CR2
       TIM1 $8 + constant TIM1_SMCR
       TIM1 $C + constant TIM1_DIER
       TIM1 $10 + constant TIM1_SR
       TIM1 $14 + constant TIM1_EGR
       TIM1 $18 + constant TIM1_CCMR1_Output
       TIM1 $18 + constant TIM1_CCMR1_Input
       TIM1 $1C + constant TIM1_CCMR2_Output
       TIM1 $1C + constant TIM1_CCMR2_Input
       TIM1 $20 + constant TIM1_CCER
       TIM1 $24 + constant TIM1_CNT
       TIM1 $28 + constant TIM1_PSC
       TIM1 $2C + constant TIM1_ARR
       TIM1 $34 + constant TIM1_CCR1
       TIM1 $38 + constant TIM1_CCR2
       TIM1 $3C + constant TIM1_CCR3
       TIM1 $40 + constant TIM1_CCR4
       TIM1 $48 + constant TIM1_DCR
       TIM1 $4C + constant TIM1_DMAR
       TIM1 $30 + constant TIM1_RCR
       TIM1 $44 + constant TIM1_BDTR
    
    $40000000 constant TIM2  
       TIM2 $0 + constant TIM2_CR1
       TIM2 $4 + constant TIM2_CR2
       TIM2 $8 + constant TIM2_SMCR
       TIM2 $C + constant TIM2_DIER
       TIM2 $10 + constant TIM2_SR
       TIM2 $14 + constant TIM2_EGR
       TIM2 $18 + constant TIM2_CCMR1_Output
       TIM2 $18 + constant TIM2_CCMR1_Input
       TIM2 $1C + constant TIM2_CCMR2_Output
       TIM2 $1C + constant TIM2_CCMR2_Input
       TIM2 $20 + constant TIM2_CCER
       TIM2 $24 + constant TIM2_CNT
       TIM2 $28 + constant TIM2_PSC
       TIM2 $2C + constant TIM2_ARR
       TIM2 $34 + constant TIM2_CCR1
       TIM2 $38 + constant TIM2_CCR2
       TIM2 $3C + constant TIM2_CCR3
       TIM2 $40 + constant TIM2_CCR4
       TIM2 $48 + constant TIM2_DCR
       TIM2 $4C + constant TIM2_DMAR
       
          $E000E000 constant NVIC  
       NVIC $4 + constant NVIC_ICTR
       NVIC $F00 + constant NVIC_STIR
       NVIC $100 + constant NVIC_ISER0
       NVIC $104 + constant NVIC_ISER1
       NVIC $180 + constant NVIC_ICER0
       NVIC $184 + constant NVIC_ICER1
       NVIC $200 + constant NVIC_ISPR0
       NVIC $204 + constant NVIC_ISPR1
       NVIC $280 + constant NVIC_ICPR0
       NVIC $284 + constant NVIC_ICPR1
       NVIC $300 + constant NVIC_IABR0
       NVIC $304 + constant NVIC_IABR1
       NVIC $400 + constant NVIC_IPR0
       NVIC $404 + constant NVIC_IPR1
       NVIC $408 + constant NVIC_IPR2
       NVIC $40C + constant NVIC_IPR3
       NVIC $410 + constant NVIC_IPR4
       NVIC $414 + constant NVIC_IPR5
       NVIC $418 + constant NVIC_IPR6
       NVIC $41C + constant NVIC_IPR7
       NVIC $420 + constant NVIC_IPR8
       NVIC $424 + constant NVIC_IPR9
       NVIC $428 + constant NVIC_IPR10
       NVIC $42C + constant NVIC_IPR11
       NVIC $430 + constant NVIC_IPR12
       NVIC $434 + constant NVIC_IPR13
       NVIC $438 + constant NVIC_IPR14
        
    $40010400 constant EXTI  
       EXTI $0 + constant EXTI_IMR
       EXTI $4 + constant EXTI_EMR
       EXTI $8 + constant EXTI_RTSR
       EXTI $C + constant EXTI_FTSR
       EXTI $10 + constant EXTI_SWIER
       EXTI $14 + constant EXTI_PR
    
	 

: bit ( n--) 1 swap lshift ; 

: baud ( u -- u )  \ calculate baud rate divider, based on current clock rate
  cpuHz @ swap / ;


: 8MHz ( -- )  \ set the main clock back to 8 MHz, keep baud rate at 115200
  0 RCC_CFGR !                    \ revert to HSI @ 8 MHz, no PLL
  $81 RCC_CR !                    \ turn off HSE and PLL, power-up value
  $18 FLASH_ACR !                 \ zero flash wait, enable half-cycle access
  8000000 cpuHz !  115200 baud USART1_BRR !  \ fix console baud rate
;

: 72MHz ( -- )  \ set the main clock to 72 MHz, keep baud rate at 115200
  $12 FLASH_ACR !                 \ two flash mem wait states
  16 bit RCC_CR bis!              \ set HSEON
  begin 17 bit RCC_CR bit@ until  \ wait for HSERDY
  1 16 lshift                     \ HSE clock is 8 MHz Xtal source for PLL
  7 18 lshift or                  \ PLL factor: 8 MHz * 9 = 72 MHz = HCLK
  4  8 lshift or                  \ PCLK1 = HCLK/2
  2 14 lshift or                  \ ADCPRE = PCLK2/6
            2 or  RCC_CFGR !      \ PLL is the system clock
  24 bit RCC_CR bis!              \ set PLLON
  begin 25 bit RCC_CR bit@ until  \ wait for PLLRDY
  \ 625 USART1_BRR !                \ fix console baud rate
    72000000 cpuHz !
115200 baud USART1_BRR !

;	   
72MHz

#require io_F100.fs \ STM32F1xx.fs
#require /opt/mecrisp/common/dump.txt


\ CPU definitions
2 13 io constant Led  \ Green LED on PC9

Led constant LedStatus \ LED used during startup
\ : ledOn  ioc! ;  \ immediate inline
\ : ledOff ios! ;  \ immediate inline 

: setupLeds
  OMODE-PP      Led  IO-mode!
  led ios! 
;
: flash-kb $1FFFF7E0 h@ ;
: cpuid    ( -- u1,u2,u3,3) $1FFFF7E8 @ $1FFFF7EC @ $1FFFF7C0 @ 3 ;	 

#require systick.fs
\ ---------------------------------------------------------

 \ Long comment
 \ If length of token is zero, end of line is reached. Fetch new line. Fetch new token.
 \ until Search for *)
\ : (* begin  token dup 0= if 2drop cr query token then 
\ 		s" *)" compare until immediate 0-foldable ;
	
\ : ledOn  ios! ;
\ : ledOff ioc! ;


: hello  sysName type ."  CpuId:"  cpuid 0 do hex. loop  ." CpuHz:" cpuHz @ 1000000 / . ." MHz" cr  ." Flash:" flash-kb .
       ." Kb Ram Free: "  $20002000 compiletoram here -  dup hex. . cr 
       ." Flash Used: " compiletoflash here $20000000 - dup hex. ." Flash Available: "  flash-kb 1024 * swap - hex. cr      
       ;
	   
: init 
     72MHz
  OMODE-PP      Led  IO-mode!  led ioc! 
    init-delay
    hello
    200 ms led ios!
   \ init
;
\ cornerstone --RewindToBasics--

cornerstone ---forthIDE/basics_bluepill.fs---
compiletoram
cr

init




