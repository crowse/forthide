\ Basic common environment for Cortex M3 processors 
\ Machine independent basic definitions that give a consistent 
\ FORTH VM and environment
\ requires registers and basic IO to be preloaded

\ At the end of loading this file for your CPU using e4th you should have a comfortable working environment
\ the environment uses flash up to about $C000 (48Kb) and 4 bytes of RAM (cpuHz)
\ Can we get rid of this by using a counter/constant in systick or RTC?

\ We should be able to duplicate this structure on any modern ARM CPU and still have space for the app

\ LedStatus ios!
#include F4_cornerstone.fs
\ #include ../Registers/STM32F40x.fs

\ : cornerstone ( Name ) ( -- )
\   <builds begin here $3FF and while 0 h, repeat
\   does>   begin dup  $3FF and while 2+   repeat 
\           eraseflashfrom
\  ;
\ include \mecrisp-stellaris-2.4.4\stm32f407\lib_registers.txt

\ VOCABULARY disassembler disassembler definitions
\ #require /opt/mecrisp/common/disassembler-m3.txt

\ FORTH DEFINITIONS
\ #require /opt/mecrisp/common/dump.txt

\ Systick-Interrupt

\ CPU definitions

: flash-kb $1FFFF7E0 h@ ;
: cpuid    ( -- u1,u2,u3,3) $1FFFF7E8 @ $1FFFF7EC @ $1FFFF7C0 @ 3 ;	


\ ---------------------------------------------------------
\ uptime and clock interfere with delays - rather use RTC
\ 0 variable upTime

\ : systick-1Hz ( -- ) cpuHz @ systick ; \ Tick every second with 8 MHz clock
\ : tick  ( -- ) 1 upTime +! ;
\ : .upTime uptime @ 3600 /mod >r . [char] : emit r> 60 /mod >r . [char] : emit r> . ; 
\ : clock ( -- ) 
\   ['] tick irq-systick !
\   systick-1Hz
\   eint
\ ;
\ ---------------------------------------------------------

 \ Long comment
 \ If length of token is zero, end of line is reached. Fetch new line. Fetch new token.
 \ until Search for *)
\ : (* begin  token dup 0= if 2drop cr query token then 
\ 		s" *)" compare until immediate 0-foldable ;
	
\ : ledOn  ios! ;
\ : ledOff ioc! ;
\ the compileto.... may mess with this all going to RAM
: hello compiletoram?  
	sysName type space cpuHz @  1000000 / . ." MHz CpuId:"  cpuid 0 do hex. loop cr
        ." Flash Kb:"  flash-kb .  cr
        ." Ram Free (192Kb): " $20030000 compiletoram   here - dup . hex. cr 
        ." Flash Free (1M) : " $100000   compiletoflash here - dup . hex. cr 
        ." Flash used: " compiletoflash here .         
        ." Ram Used: "   compiletoram   here $20000000 - . cr
	if compiletoram else compiletoflash then ;

\ this needs to come out as it does not use IRQs, and polls the NVIC_ST_CURRENT_R register
\ I then need to rewrite for multitasking 

\ #require systick.fs


: init 
    initleds 
    maxCpuHz
  \   setupLeds
  \  init-delay
    hello
  \   100 ms LedStatus ledOff
;
init
\ cornerstone --RewindToBasics--



