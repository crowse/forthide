
\ Delay with Systick-Timer
$E000E010 constant NVIC  
 NVIC $0 + constant NVIC_ST_CTRL_R
 NVIC $4 + constant NVIC_ST_RELOAD_R      
 NVIC $8 + constant NVIC_ST_CURRENT_R

\  to use systick else where simply ' proc irq-systick !
\ provide a dummy so it works, can always override / enhance
\ BEWARE!!!! The systick config remains after boot, the handlaer must be in place for reboot
0 variable systicks
: systickHandler 1 systicks +! ;

\ : init ['] systickHandler irq-systick ! ;

\ init
8000000 variable cpuHz

: systick ( ticks -- )   \ typically cpuHz @ 1000000 / systick
    NVIC_ST_RELOAD_R ! \ How many ticks between interrupts ?
  \ %111 NVIC_ST_CTRL_R ! \ Enable the systick with interrupt.
   %101 NVIC_ST_CTRL_R !  \ no interrupts
;

: init-delay ( -- )
    ['] systickHandler irq-systick !
                                \ Start free running Systick Timer without Interrupts 
                                \ Disable SysTick during setup
    0 NVIC_ST_CTRL_R !
                                \ Maximum reload value for 24 bit timer
    $00FFFFFF NVIC_ST_RELOAD_R !
                                \ Any write to current clears it
    0 NVIC_ST_CURRENT_R !
    
    cpuHz @ 1000000 / 1- systick 
    \ Enable SysTick with 8MHz clock
    \ %101 NVIC_ST_CTRL_R !       \ Use %101 instead for core clock (always 8MHz?)
    eint
;

\ : delay-ticks ( ticks -- ) \  Tick = 1/8MHz = 125 ns
\  NVIC_ST_CURRENT_R @ \ Get the starting time
\  swap -              \ Subtract ticks to wait

\  dup 0< if  \ If difference is negative...
\           $00FFFFFF and \ Convert to 24-bit subtraction to calculate value after rollover
                    \ Better might be to look at bit 16 of CTRL_R which indicates rollover occurred 
             \ begin NVIC_ST_CTRL_R @ 16 bit AND until 
\           begin $800000 NVIC_ST_CURRENT_R bit@ until \ Wait for next rollover
\         then

\  begin
\    dup
\    NVIC_ST_CURRENT_R @ \ Get current time
\    ( finish finish current )
\    u>= \ Systick counts backwards
\  until
\  drop
\ ;

\  These delays work in us up to 4296 seconds (71 minutes 36 seconds)
: us ( us -- ) dup systicks @ + 
        2dup > if begin pause systicks @ $80000000 and until then  
        begin pause dup systicks @ < until 2drop ;  \ 8 * delay-ticks ;
: ms ( ms -- ) 1000 * us ;                                       \ 0 ?do 8000 delay-ticks loop ;



