\ This loads and customises basics_m3 for the VLDiscovery board
\ forgetram
\ eraseFlash

compiletoflash

8000000 variable cpuHz
\ 8000000 variable clock-hz  \ the system clock is 8 MHz after reset

5 constant numGPIOPorts
: sysName s" BluePill Quad" ;

#require /opt/mecrisp/reg/STM32F103xx.svd.reg_memmap.txt
#require ioSTM32F1xx.fs

\ 0 0 io constant button    \ User button on PA0
\ 2 8 io constant LedBlue   \ Blue  LED on PC8
2 13 io constant Led  \ Green LED on PC9

Led constant LedStatus \ LED used during startup
: ledOn  ioc! ;  immediate inline
: ledOff ios! ;  immediate inline 

: setupLeds
  OMODE-PP      Led  IO-mode!
  led LedOff 
;

setupLeds 
  ( 1 24 lshift ) 24 bit constant PLLON
  ( 1 25 lshift ) 25 bit constant PLLRDY
  ( 1 16 lshift ) 16 bit constant HSEON
  ( 1 17 lshift ) 17 bit constant HSERDY


: baud ( u -- u )  \ calculate baud rate divider, based on current clock rate
  cpuHz @ swap / ;



: 8MHz ( -- )  \ set the main clock back to 8 MHz, keep baud rate at 115200
  0 RCC_CFGR !                    \ revert to HSI @ 8 MHz, no PLL
  $81 RCC_CR !                    \ turn off HSE and PLL, power-up value
  $18 FLASH_ACR !                 \ zero flash wait, enable half-cycle access
  8000000 cpuHz !  115200 baud USART1_BRR !  \ fix console baud rate
;

: 72MHz ( -- )  \ set the main clock to 72 MHz, keep baud rate at 115200
  8MHz                            \ make sure the PLL is off
  $12 FLASH_ACR !                 \ two flash mem wait states
  16 bit RCC_CR bis!              \ set HSEON
  begin 17 bit RCC_CR bit@ until  \ wait for HSERDY
  1 16 lshift                     \ HSE clock is 8 MHz Xtal source for PLL
  7 18 lshift or                  \ PLL factor: 8 MHz * 9 = 72 MHz = HCLK
  4  8 lshift or                  \ PCLK1 = HCLK/2
  2 14 lshift or                  \ ADCPRE = PCLK2/6
            2 or  RCC_CFGR !      \ PLL is the system clock
  24 bit RCC_CR bis!              \ set PLLON
  begin 25 bit RCC_CR bit@ until  \ wait for PLLRDY
  72000000 cpuHz !  115200 baud USART1_BRR !  \ fix console baud rate
;

: maxCpuHz 72MHz 72000000 cpuHz ! ; 

maxCpuHz

#require basics_bluepill.fs
Led ledOff



