\ This loads and customises basics_m3 for the VLDiscovery board

forgetram
\ eraseFlash

compiletoflash

8000000 variable cpuHz
5 constant numGPIOPorts
: sysName s" Mecrisp VLDiscovery" ;
100 constant cpu

\ #require /opt/mecrisp/reg/STM32F100xx.svd.reg_memmap.txt
\ #require ../Registers/STM32F100xx.reg.txt
include d:/git/registers/STM32F100xx.svd.reg_memmap.txt

\ #require io_F100.fs
include io_F100.fs

0 0 io constant button    \ User button on PA0
2 8 io constant LedBlue   \ Blue  LED on PC8
2 9 io constant LedGreen  \ Green LED on PC9

2 8 io constant Led 

LedBlue constant LedStatus \ LED used during startup

: ledOn ioc! ;
: ledOff ios! ;

: setupLeds
  OMODE-PP      LedBlue  IO-mode!
  OMODE-PP      LedGreen IO-mode!
  IMODE-FLOAT   button   IO-mode!
  LedGreen ios!  
;

setupLeds 

\ $40021000 constant RCC_Base

\ $40013808 constant USART1_BRR
\ RCC_Base $00 + constant RCC_CR
  ( 1 24 lshift ) 24 bit constant PLLON
  ( 1 25 lshift ) 25 bit constant PLLRDY
  ( 1 16 lshift ) 16 bit constant HSEON
  ( 1 17 lshift ) 17 bit constant HSERDY

\ RCC $04 + constant RCC_PLLCFGR
   1 16 lshift constant PLLSRC

: baud ( u -- u )  \ calculate baud rate divider, based on current clock rate
  cpuhz @ swap 2/ / ;

: 8MHz ( -- )  \ set the main clock back to 8 MHz, keep baud rate at 115200
  0 RCC_CFGR !                    \ revert to HSI @ 8 MHz, no PLL
  $81 RCC_CR !                    \ turn off HSE and PLL, power-up value
  $18 FLASH_ACR !                 \ zero flash wait, enable half-cycle access
  8000000 cpuHz !  115200 baud USART1_BRR !  \ fix console baud rate
;

: 24MHz ( -- )
  HSEON RCC_CR bis!     \ switch HSE ON
                        \ Wait for HSE to be ready
  begin HSERDY RCC_CR bit@ until 
  PLLSRC                \ HSE clock is 8 MHz Xtal source
  1  18 lshift or       \ PLL multiplication factor
                        \ 4 = 8 MHz * 6 = 48 MHz = HCLK
                        \ 1 = 8MHz * 3 = 24MHz = HCLK = max CPU frequency
  4  11 lshift or       \ PCLK2 = HCLK/2
  4  08 lshift or       \ PCLK1 = HCLK/2		    
  1  14 lshift or       \ ADCPRE = PCLK2/4
  2  or                 \ PLL is the system clock
  RCC_CFGR !
  PLLON RCC_CR bis!     \ switch PLL ON
                        \ Wait for PLL to lock:
  begin PLLRDY RCC_CR bit@ until  
    
  $068 USART1_BRR !     \ Set Baud rate divider for 115200 Baud at PCLK2=24MHz (13.02)
  24000000 cpuHz ! \ 115200 baud USART1_BRR ! 
;

: maxCpuHz 24MHz 24000000 cpuHz ! ; 

maxCpuHz
: flash-kb $1FFFF7E0 h@ ;
: cpuid    ( -- u1,u2,u3,3) $1FFFF7E8 @ $1FFFF7EC @ $1FFFF7C0 @ 3 ;	 

\ #require systick.fs
include systick.fs
LedGreen ioc!
: bit 1 swap lshift ;
: NVIC_EnableIRQ  ( IRQn--)  dup 32 < if BIT NVIC_ISER0 ELSE 32 - BIT NVIC_ISER1 THEN BIS! ;  \ Set Int enable bit
: NVIC_DisableIRQ ( IRQn--)  dup 32 < if BIT NVIC_ISER0 ELSE 32 - BIT NVIC_ISER1 THEN BIC! ;   		  

 8  constant EXTI2_IRQn
 23 constant EXTI5_Irqn
 28 constant TIM2_IRQn
 40 constant EXTI10_Irqn



: hello  sysName type ."  CpuId:"  cpuid 0 do hex. loop  ." CpuHz:" cpuHz @ 1000000 / . ." MHz" cr  ." Flash:" flash-kb .
       ." Kb Ram Free: "  $20002000 compiletoram here -  dup hex. . cr 
       ." Flash Used: " compiletoflash here $20000000 - dup hex. ." Flash Available: "  flash-kb 1024 * swap - hex. cr      
       ;
	   
: init_IDE 
     maxCpuHz
    OMODE-PP      Led  IO-mode!  led ioc! 
    
    init-delay
    hello
    200 ms led ios!
   \ init
;
: cornerstone ( Name ) ( -- )   
    <builds begin here $3FF and while 0 h, repeat
   does>   begin dup  $3FF and while 2+   repeat
          eraseflashfrom
  ;

cornerstone ---forthIDE/VLDiscovery---
compiletoram
hello
