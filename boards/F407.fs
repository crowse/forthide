\ STM32F407 Discovery 
\ 168MHz fcpu settings
\ standard clock settings
\ uart 115k2
\ by Igor de om1zz, 2015
\ no warranties of any kind
\ @TODO:  The addresses in io.fs are relevant to STM32F1xx only, dont work for F4
\ @TODO:  Build an XML parser into forthIDE to load current CPU registers 

\ VOCABULARY CPU CPU DEFINITIONS
forgetram
compiletoflash
8000000 variable cpuHz

$40023800 constant RCC_Base
 
$40023C00 constant Flash_ACR \ Flash Access Control Register
$40004408 constant USART2_BRR

5 CONSTANT numGPIOPorts

\ f (VCO clock) = f (PLL clock input) * (PLLN/PLLM)
\ f (PLL general clock output) = F (VCO clock) / PLLP
\ f (USB, RNG und andere) = f (VCO clock) / PLLQ

RCC_Base $00 + constant RCC_CR
  1 24 lshift constant PLLON
  1 25 lshift constant PLLRDY

RCC_Base $04 + constant RCC_PLLCRGR
   1 22 lshift constant PLLSRC

RCC_Base $08 + constant RCC_CFGR

: baud ( u -- u )  \ calculate baud rate divider, based on current clock rate
  cpuHz @ swap / 4  / ;



: 8MHz ( -- )  \ set the main clock back to 8 MHz, keep baud rate at 115200
  0 RCC_CFGR !                    \ revert to HSI @ 8 MHz, no PLL
  $81 RCC_CR !                    \ turn off HSE and PLL, power-up value
  $18 FLASH_ACR !                 \ zero flash wait, enable half-cycle access
  8000000 cpuHz !  115200 baud USART2_BRR !  \ fix console baud rate
;



: 168MHz ( -- )

  \ Set Flash waitstates !
  $103 Flash_ACR !   \ 3 Waitstates for 120 MHz with more than 2.7 V Vcc, Prefetch buffer enabled.

  PLLSRC          \ HSE clock as 8 MHz source

  8  0 lshift or  \ PLLM Division factor for main PLL and audio PLL input clock 
                  \ 8 MHz / 8 =  1 MHz. Divider before VCO. Frequency entering VCO to be between 1 and 2 MHz.

336  6 lshift or  \ PLLN Main PLL multiplication factor for VCO - between 192 and 432 MHz
                  \ 1 MHz * 336 = 336 MHz

  7 24 lshift or  \ PLLQ = 7, 336 MHz / 8 = 48 MHz

  0 16 lshift or  \ PLLP Division factor for main system clock
                  \ 0: /2  1: /4  2: /6  3: /8
                  \ 336 MHz / 2 = 168 MHz 
  RCC_PLLCRGR !

  PLLON RCC_CR bis!
    \ Wait for PLL to lock:
    begin PLLRDY RCC_CR bit@ until

  2                 \ Set PLL as clock source
  %101 10 lshift or \ APB  Low speed prescaler (APB1) - Max 42 MHz ! Here 168/4 MHz = 42 MHz.
  %100 13 lshift or \ APB High speed prescaler (APB2) - Max 84 MHz ! Here 168/2 MHz = 84 MHz.
  RCC_CFGR !
  168000000 cpuHz ! 
  \ $16D 2/ usart2_brr !
  \ $16d USART2_BRR ! \ Set Baud rate divider for 115200 Baud at 42 MHz. 22.786
   115200 baud USART2_BRR !
;

: maxCPUHz  168MHz 168000000 cpuHz ! 115200 baud USART2_BRR ! ;
\ maxCpuHz


\ 115200 baud hex. 

\ 8000000 variable clock-hz  \ the system clock is 8 MHz after reset

: sysName s" F407 Discovery" ;

\  ************************************************************ LAPTOP INCLUDES
\ see forth-cnc boards

\ #include ./io_F4.fs

\ ONLY FORTH ALSO CPU 

\ FORTH DEFINITIONS 
 3 12 io constant LED_GREEN
 3 13 io constant LED_ORANGE
 3 14 io constant LED_RED
 3 15 io constant LED_BLUE

\ LED1 constant LEDStatus

 : initLeds 
 	OMODE-PP LED_GREEN  IO-MODE!  
 	OMODE-PP LED_ORANGE IO-MODE!  
 	OMODE-PP LED_RED    IO-MODE!  
 	OMODE-PP LED_BLUE   IO-MODE!  ;
\ FORTH
\ initLeds

\ : bit ( n --) 1 swap lshift ;
\ bit# ( n --m) \ find which power of 2 is set, return -1 if out of range
\  converse of bit  

: bit#  dup 1- 31 bit u< if 
        32 0 do dup 1 and if drop i leave then 2/ loop
        else drop -1 then ;


: eraseflashfrom ." EraseFlashFrom not implemented ... Use eraseflash and # include forthide/boards/F407.fs :" . cr ;
: list ( -- ) cr dictionarystart begin dup 6 + ctype space dictionarynext until drop cr ;
 
: listc  ." Enter first char:" key cr dictionarystart 
          begin dup 6 + \ at count
               dup 1+ c@  3 pick =  
               if ctype cr else drop then  
          dictionarynext until  2drop cr ;

\ #require basics_m3.fs

\ compiletoram
