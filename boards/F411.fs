\ requires registers.txt
\ requires io.fs

forgetram
#require basisdefinitions.txt
#require io.txt

\ cnc-steppers
\ compiletoflash

\ The basic Multi stepper motor driver

\ these definitions are for VL discovery and flash the LEDS on X and Y
\ 2 8 io constant X_STEP
\ 2 9 io constant Y_STEP
\ 0 6 io constant Z_STEP

\ 0 1 io constant X_DIR
\ 0 2 io constant Y_DIR
\ 0 3 io constant Z_DIR

\ 0 7 io constant X_ENABLE
\ 1 8 io constant Y_ENABLE
\ 1 9 io constant Z_ENABLE

\ These are the deifinitions for the breakout board on Rockwell with VL Discovery

1 2 io constant X_STOP   	
1 3 io constant X_ENABLE 	
0 4 io constant X_DIR
0 5 io constant X_STEP    
0 6 io constant X_ADC     	 

0 7 io constant Y_STOP		
2 4 io constant Y_ENABLE
2 5 io constant Y_DIR
1 0 io constant Y_STEP 		
1 1 io constant Y_ADC		

1 10 io constant Z_STOP		
1 11 io constant Z_ENABLE	
1 12 io constant Z_DIR		
1 13 io constant Z_STEP		
1 14 io constant Z_ADC      

0 8 io constant U_STOP		
0 11 io constant U_ENABLE	
0 12 io constant U_DIR		 
2 10 io constant U_STEP		 
2 11 io constant U_ADC       

2 12 io constant V_STOP		 
3 2  io constant V_ENABLE	 
1 5  io constant V_DIR		
1 8 io constant V_STEP		
1 9 io constant V_ADC       

  
: setup-pins1 omode-pp X_STEP io-mode!
	omode-pp Y_STEP io-mode!
	omode-pp Z_STEP io-mode!

	omode-pp X_DIR io-mode!
	omode-pp Y_DIR io-mode!
	omode-pp Z_DIR io-mode!

	omode-pp X_ENABLE io-mode!
	omode-pp Y_ENABLE io-mode!
	omode-pp Z_ENABLE io-mode!

    imode-PULL X_STOP io-mode!
    imode-PULL Y_STOP io-mode!
    imode-PULL Z_STOP io-mode!

    imode-ADC  X_ADC io-mode!
    imode-ADC  Y_ADC io-mode!
    imode-ADC  Z_ADC io-mode!

	X_STEP ioc! X_DIR ioc! X_ENABLE ios!
	Y_STEP ioc! Y_DIR ioc! Y_ENABLE ios!
	Z_STEP ioc! Z_DIR ioc! Z_ENABLE ios!	
;

setup-pins1

\ #require cnc-steppers.txt
