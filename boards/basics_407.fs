\ Basic common environment for Cortex M3 processors 
\ Machine independent basic definitions that give a consistent 
\ FORTH VM and environment
\ requires registers and basic IO to be preloaded

\ At the end of loading this file for your CPU using e4th you should have a comfortable working environment
\ the environment uses flash up to about $C000 (48Kb) and 4 bytes of RAM (cpuHz)
\ Can we get rid of this by using a counter/constant in systick or RTC?

\ We should be able to duplicate this structure on any modern ARM CPU and still have space for the app

\ LedStatus ios!

\  Cant use cornerstone as is because doesnt support eraseflashfrom
\ : cornerstone ( Name ) ( -- )
\   <builds begin here $3FF and while 0 h, repeat
\   does>   begin dup  $3FF and while 2+   repeat 
\           eraseflashfrom
\  ;
\ include \mecrisp-stellaris-2.4.4\stm32f407\lib_registers.txt

\ VOCABULARY disassembler disassembler definitions
\ #require /opt/mecrisp/common/disassembler-m3.txt

\ FORTH DEFINITIONS
\ #require /opt/mecrisp/common/dump.txt

\ Systick-Interrupt

\ CPU definitions
\     0x1FFF7A10  for F4
\ for $1FFFF7E8 for F1

: flash-kb $1FFFF7E0 h@ ;
: cpuid    ( -- u1,u2,u3,3) $1FFFF7E8 @ $1FFFF7EC @ $1FFFF7C0 @ 3 ;	

: .cpuid_f4 $1FFF7A10 @ ." Implementer:" dup 24 rshift .
        ."  Variant:" dup 20 rshift $F and .
        ."  Part Number:" dup 4 rshift $FFF and .
        ."  Revision:" $f and . ; 

\ ---------------------------------------------------------
\ uptime and clock interfere with delays - rather use RTC
\ 0 variable upTime

\ : systick-1Hz ( -- ) cpuHz @ systick ; \ Tick every second with 8 MHz clock
\ : tick  ( -- ) 1 upTime +! ;
\ : .upTime uptime @ 3600 /mod >r . [char] : emit r> 60 /mod >r . [char] : emit r> . ; 
\ : clock ( -- ) 
\   ['] tick irq-systick !
\   systick-1Hz
\   eint
\ ;
\ ---------------------------------------------------------

 \ Long comment
 \ If length of token is zero, end of line is reached. Fetch new line. Fetch new token.
 \ until Search for *)
\ : (* begin  token dup 0= if 2drop cr query token then 
\ 		s" *)" compare until immediate 0-foldable ;
	
\ : ledOn  ios! ;
\ : ledOff ioc! ;
\ the compileto.... may mess with this all going to RAM
: hello  compiletoram? 
		sysName type ."  CpuId:"  cpu . cpuid 0 do hex. loop ."  Flash:"  flash-kb .
        ." Kb Ram Free: "  $20002000 compiletoram here - . cr 
        ." Flash Used: " compiletoflash here $20000000 - dup hex. cr
		." Flash Available: "  flash-kb 1024 * - dup hex. ." x (" . ." ) " cr 
		if compiletoram else compiletoflash then ;

#require systick.fs
	   
: init 
  \   LedStatus ledOn
    maxCpuHz
  \   setupLeds
    init-delay
    hello
  \   100 ms LedStatus ledOff
;
init
\ cornerstone --RewindToBasics--



