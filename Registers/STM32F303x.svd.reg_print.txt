\ STM32F303x Register Print file for Mecrisp-Stellaris Forth by Matthias Koch
\ By Terry Porter "terry@tjporter.com.au"

: GPIOA. cr
." GPIOA_MODER: " GPIOA_MODER @ hex. cr
." GPIOA_OTYPER: " GPIOA_OTYPER @ hex. cr
." GPIOA_OSPEEDR: " GPIOA_OSPEEDR @ hex. cr
." GPIOA_PUPDR: " GPIOA_PUPDR @ hex. cr
." GPIOA_IDR: " GPIOA_IDR @ hex. cr
." GPIOA_ODR: " GPIOA_ODR @ hex. cr
." GPIOA_BSRR: " GPIOA_BSRR @ hex. cr
." GPIOA_LCKR: " GPIOA_LCKR @ hex. cr
." GPIOA_AFRL: " GPIOA_AFRL @ hex. cr
." GPIOA_AFRH: " GPIOA_AFRH @ hex. cr
." GPIOA_BRR: " GPIOA_BRR @ hex. cr
;

: GPIOB. cr
." GPIOB_MODER: " GPIOB_MODER @ hex. cr
." GPIOB_OTYPER: " GPIOB_OTYPER @ hex. cr
." GPIOB_OSPEEDR: " GPIOB_OSPEEDR @ hex. cr
." GPIOB_PUPDR: " GPIOB_PUPDR @ hex. cr
." GPIOB_IDR: " GPIOB_IDR @ hex. cr
." GPIOB_ODR: " GPIOB_ODR @ hex. cr
." GPIOB_BSRR: " GPIOB_BSRR @ hex. cr
." GPIOB_LCKR: " GPIOB_LCKR @ hex. cr
." GPIOB_AFRL: " GPIOB_AFRL @ hex. cr
." GPIOB_AFRH: " GPIOB_AFRH @ hex. cr
." GPIOB_BRR: " GPIOB_BRR @ hex. cr
;

: GPIOC. cr
;

: GPIOD. cr
;

: GPIOF. cr
;

: TSC. cr
." TSC_CR: " TSC_CR @ hex. cr
." TSC_IER: " TSC_IER @ hex. cr
." TSC_ICR: " TSC_ICR @ hex. cr
." TSC_ISR: " TSC_ISR @ hex. cr
." TSC_IOHCR: " TSC_IOHCR @ hex. cr
." TSC_IOASCR: " TSC_IOASCR @ hex. cr
." TSC_IOSCR: " TSC_IOSCR @ hex. cr
." TSC_IOCCR: " TSC_IOCCR @ hex. cr
." TSC_IOGCSR: " TSC_IOGCSR @ hex. cr
." TSC_IOG1CR: " TSC_IOG1CR @ hex. cr
." TSC_IOG2CR: " TSC_IOG2CR @ hex. cr
." TSC_IOG3CR: " TSC_IOG3CR @ hex. cr
." TSC_IOG4CR: " TSC_IOG4CR @ hex. cr
." TSC_IOG5CR: " TSC_IOG5CR @ hex. cr
." TSC_IOG6CR: " TSC_IOG6CR @ hex. cr
." TSC_IOG7CR: " TSC_IOG7CR @ hex. cr
." TSC_IOG8CR: " TSC_IOG8CR @ hex. cr
;

: CRC. cr
." CRC_DR: " CRC_DR @ hex. cr
." CRC_IDR: " CRC_IDR @ hex. cr
." CRC_CR: " CRC_CR @ hex. cr
." CRC_INIT: " CRC_INIT @ hex. cr
." CRC_POL: " CRC_POL @ hex. cr
;

: Flash. cr
." Flash_ACR: " Flash_ACR @ hex. cr
." Flash_KEYR: " Flash_KEYR @ hex. cr
." Flash_OPTKEYR: " Flash_OPTKEYR @ hex. cr
." Flash_SR: " Flash_SR @ hex. cr
." Flash_CR: " Flash_CR @ hex. cr
." Flash_AR: " Flash_AR @ hex. cr
." Flash_OBR: " Flash_OBR @ hex. cr
." Flash_WRPR: " Flash_WRPR @ hex. cr
;

: RCC. cr
." RCC_CR: " RCC_CR @ hex. cr
." RCC_CFGR: " RCC_CFGR @ hex. cr
." RCC_CIR: " RCC_CIR @ hex. cr
." RCC_APB2RSTR: " RCC_APB2RSTR @ hex. cr
." RCC_APB1RSTR: " RCC_APB1RSTR @ hex. cr
." RCC_AHBENR: " RCC_AHBENR @ hex. cr
." RCC_APB2ENR: " RCC_APB2ENR @ hex. cr
." RCC_APB1ENR: " RCC_APB1ENR @ hex. cr
." RCC_BDCR: " RCC_BDCR @ hex. cr
." RCC_CSR: " RCC_CSR @ hex. cr
." RCC_AHBRSTR: " RCC_AHBRSTR @ hex. cr
." RCC_CFGR2: " RCC_CFGR2 @ hex. cr
." RCC_CFGR3: " RCC_CFGR3 @ hex. cr
;

: DMA1. cr
." DMA1_ISR: " DMA1_ISR @ hex. cr
." DMA1_IFCR: " DMA1_IFCR @ hex. cr
." DMA1_CCR1: " DMA1_CCR1 @ hex. cr
." DMA1_CNDTR1: " DMA1_CNDTR1 @ hex. cr
." DMA1_CPAR1: " DMA1_CPAR1 @ hex. cr
." DMA1_CMAR1: " DMA1_CMAR1 @ hex. cr
." DMA1_CCR2: " DMA1_CCR2 @ hex. cr
." DMA1_CNDTR2: " DMA1_CNDTR2 @ hex. cr
." DMA1_CPAR2: " DMA1_CPAR2 @ hex. cr
." DMA1_CMAR2: " DMA1_CMAR2 @ hex. cr
." DMA1_CCR3: " DMA1_CCR3 @ hex. cr
." DMA1_CNDTR3: " DMA1_CNDTR3 @ hex. cr
." DMA1_CPAR3: " DMA1_CPAR3 @ hex. cr
." DMA1_CMAR3: " DMA1_CMAR3 @ hex. cr
." DMA1_CCR4: " DMA1_CCR4 @ hex. cr
." DMA1_CNDTR4: " DMA1_CNDTR4 @ hex. cr
." DMA1_CPAR4: " DMA1_CPAR4 @ hex. cr
." DMA1_CMAR4: " DMA1_CMAR4 @ hex. cr
." DMA1_CCR5: " DMA1_CCR5 @ hex. cr
." DMA1_CNDTR5: " DMA1_CNDTR5 @ hex. cr
." DMA1_CPAR5: " DMA1_CPAR5 @ hex. cr
." DMA1_CMAR5: " DMA1_CMAR5 @ hex. cr
." DMA1_CCR6: " DMA1_CCR6 @ hex. cr
." DMA1_CNDTR6: " DMA1_CNDTR6 @ hex. cr
." DMA1_CPAR6: " DMA1_CPAR6 @ hex. cr
." DMA1_CMAR6: " DMA1_CMAR6 @ hex. cr
." DMA1_CCR7: " DMA1_CCR7 @ hex. cr
." DMA1_CNDTR7: " DMA1_CNDTR7 @ hex. cr
." DMA1_CPAR7: " DMA1_CPAR7 @ hex. cr
." DMA1_CMAR7: " DMA1_CMAR7 @ hex. cr
;

: TIM2. cr
." TIM2_CR1: " TIM2_CR1 @ hex. cr
." TIM2_CR2: " TIM2_CR2 @ hex. cr
." TIM2_SMCR: " TIM2_SMCR @ hex. cr
." TIM2_DIER: " TIM2_DIER @ hex. cr
." TIM2_SR: " TIM2_SR @ hex. cr
." TIM2_EGR: " TIM2_EGR @ hex. cr
." TIM2_CCMR1_Output: " TIM2_CCMR1_Output @ hex. cr
." TIM2_CCMR1_Input: " TIM2_CCMR1_Input @ hex. cr
." TIM2_CCMR2_Output: " TIM2_CCMR2_Output @ hex. cr
." TIM2_CCMR2_Input: " TIM2_CCMR2_Input @ hex. cr
." TIM2_CCER: " TIM2_CCER @ hex. cr
." TIM2_CNT: " TIM2_CNT @ hex. cr
." TIM2_PSC: " TIM2_PSC @ hex. cr
." TIM2_ARR: " TIM2_ARR @ hex. cr
." TIM2_CCR1: " TIM2_CCR1 @ hex. cr
." TIM2_CCR2: " TIM2_CCR2 @ hex. cr
." TIM2_CCR3: " TIM2_CCR3 @ hex. cr
." TIM2_CCR4: " TIM2_CCR4 @ hex. cr
." TIM2_DCR: " TIM2_DCR @ hex. cr
." TIM2_DMAR: " TIM2_DMAR @ hex. cr
;

: TIM3. cr
;

: TIM15. cr
." TIM15_CR1: " TIM15_CR1 @ hex. cr
." TIM15_CR2: " TIM15_CR2 @ hex. cr
." TIM15_SMCR: " TIM15_SMCR @ hex. cr
." TIM15_DIER: " TIM15_DIER @ hex. cr
." TIM15_SR: " TIM15_SR @ hex. cr
." TIM15_EGR: " TIM15_EGR @ hex. cr
." TIM15_CCMR1_Output: " TIM15_CCMR1_Output @ hex. cr
." TIM15_CCMR1_Input: " TIM15_CCMR1_Input @ hex. cr
." TIM15_CCER: " TIM15_CCER @ hex. cr
." TIM15_CNT: " TIM15_CNT @ hex. cr
." TIM15_PSC: " TIM15_PSC @ hex. cr
." TIM15_ARR: " TIM15_ARR @ hex. cr
." TIM15_RCR: " TIM15_RCR @ hex. cr
." TIM15_CCR1: " TIM15_CCR1 @ hex. cr
." TIM15_CCR2: " TIM15_CCR2 @ hex. cr
." TIM15_BDTR: " TIM15_BDTR @ hex. cr
." TIM15_DCR: " TIM15_DCR @ hex. cr
." TIM15_DMAR: " TIM15_DMAR @ hex. cr
;

: TIM16. cr
." TIM16_CR1: " TIM16_CR1 @ hex. cr
." TIM16_CR2: " TIM16_CR2 @ hex. cr
." TIM16_DIER: " TIM16_DIER @ hex. cr
." TIM16_SR: " TIM16_SR @ hex. cr
." TIM16_EGR: " TIM16_EGR @ hex. cr
." TIM16_CCMR1_Output: " TIM16_CCMR1_Output @ hex. cr
." TIM16_CCMR1_Input: " TIM16_CCMR1_Input @ hex. cr
." TIM16_CCER: " TIM16_CCER @ hex. cr
." TIM16_CNT: " TIM16_CNT @ hex. cr
." TIM16_PSC: " TIM16_PSC @ hex. cr
." TIM16_ARR: " TIM16_ARR @ hex. cr
." TIM16_RCR: " TIM16_RCR @ hex. cr
." TIM16_CCR1: " TIM16_CCR1 @ hex. cr
." TIM16_BDTR: " TIM16_BDTR @ hex. cr
." TIM16_DCR: " TIM16_DCR @ hex. cr
." TIM16_DMAR: " TIM16_DMAR @ hex. cr
." TIM16_OR: " TIM16_OR @ hex. cr
;

: TIM17. cr
." TIM17_CR1: " TIM17_CR1 @ hex. cr
." TIM17_CR2: " TIM17_CR2 @ hex. cr
." TIM17_DIER: " TIM17_DIER @ hex. cr
." TIM17_SR: " TIM17_SR @ hex. cr
." TIM17_EGR: " TIM17_EGR @ hex. cr
." TIM17_CCMR1_Output: " TIM17_CCMR1_Output @ hex. cr
." TIM17_CCMR1_Input: " TIM17_CCMR1_Input @ hex. cr
." TIM17_CCER: " TIM17_CCER @ hex. cr
." TIM17_CNT: " TIM17_CNT @ hex. cr
." TIM17_PSC: " TIM17_PSC @ hex. cr
." TIM17_ARR: " TIM17_ARR @ hex. cr
." TIM17_RCR: " TIM17_RCR @ hex. cr
." TIM17_CCR1: " TIM17_CCR1 @ hex. cr
." TIM17_BDTR: " TIM17_BDTR @ hex. cr
." TIM17_DCR: " TIM17_DCR @ hex. cr
." TIM17_DMAR: " TIM17_DMAR @ hex. cr
;

: USART1. cr
." USART1_CR1: " USART1_CR1 @ hex. cr
." USART1_CR2: " USART1_CR2 @ hex. cr
." USART1_CR3: " USART1_CR3 @ hex. cr
." USART1_BRR: " USART1_BRR @ hex. cr
." USART1_GTPR: " USART1_GTPR @ hex. cr
." USART1_RTOR: " USART1_RTOR @ hex. cr
." USART1_RQR: " USART1_RQR @ hex. cr
." USART1_ISR: " USART1_ISR @ hex. cr
." USART1_ICR: " USART1_ICR @ hex. cr
." USART1_RDR: " USART1_RDR @ hex. cr
." USART1_TDR: " USART1_TDR @ hex. cr
;

: USART2. cr
;

: USART3. cr
;

: SPI1. cr
." SPI1_CR1: " SPI1_CR1 @ hex. cr
." SPI1_CR2: " SPI1_CR2 @ hex. cr
." SPI1_SR: " SPI1_SR @ hex. cr
." SPI1_DR: " SPI1_DR @ hex. cr
." SPI1_CRCPR: " SPI1_CRCPR @ hex. cr
." SPI1_RXCRCR: " SPI1_RXCRCR @ hex. cr
." SPI1_TXCRCR: " SPI1_TXCRCR @ hex. cr
." SPI1_I2SCFGR: " SPI1_I2SCFGR @ hex. cr
." SPI1_I2SPR: " SPI1_I2SPR @ hex. cr
;

: EXTI. cr
." EXTI_IMR1: " EXTI_IMR1 @ hex. cr
." EXTI_EMR1: " EXTI_EMR1 @ hex. cr
." EXTI_RTSR1: " EXTI_RTSR1 @ hex. cr
." EXTI_FTSR1: " EXTI_FTSR1 @ hex. cr
." EXTI_SWIER1: " EXTI_SWIER1 @ hex. cr
." EXTI_PR1: " EXTI_PR1 @ hex. cr
." EXTI_IMR2: " EXTI_IMR2 @ hex. cr
." EXTI_EMR2: " EXTI_EMR2 @ hex. cr
." EXTI_RTSR2: " EXTI_RTSR2 @ hex. cr
." EXTI_FTSR2: " EXTI_FTSR2 @ hex. cr
." EXTI_SWIER2: " EXTI_SWIER2 @ hex. cr
." EXTI_PR2: " EXTI_PR2 @ hex. cr
;

: COMP. cr
." COMP_COMP1_CSR: " COMP_COMP1_CSR @ hex. cr
." COMP_COMP2_CSR: " COMP_COMP2_CSR @ hex. cr
." COMP_COMP3_CSR: " COMP_COMP3_CSR @ hex. cr
." COMP_COMP4_CSR: " COMP_COMP4_CSR @ hex. cr
." COMP_COMP5_CSR: " COMP_COMP5_CSR @ hex. cr
." COMP_COMP6_CSR: " COMP_COMP6_CSR @ hex. cr
." COMP_COMP7_CSR: " COMP_COMP7_CSR @ hex. cr
;

: PWR. cr
." PWR_CR: " PWR_CR @ hex. cr
." PWR_CSR: " PWR_CSR @ hex. cr
;

: CAN. cr
." CAN_MCR: " CAN_MCR @ hex. cr
." CAN_MSR: " CAN_MSR @ hex. cr
." CAN_TSR: " CAN_TSR @ hex. cr
." CAN_RF0R: " CAN_RF0R @ hex. cr
." CAN_RF1R: " CAN_RF1R @ hex. cr
." CAN_IER: " CAN_IER @ hex. cr
." CAN_ESR: " CAN_ESR @ hex. cr
." CAN_BTR: " CAN_BTR @ hex. cr
." CAN_TI0R: " CAN_TI0R @ hex. cr
." CAN_TDT0R: " CAN_TDT0R @ hex. cr
." CAN_TDL0R: " CAN_TDL0R @ hex. cr
." CAN_TDH0R: " CAN_TDH0R @ hex. cr
." CAN_TI1R: " CAN_TI1R @ hex. cr
." CAN_TDT1R: " CAN_TDT1R @ hex. cr
." CAN_TDL1R: " CAN_TDL1R @ hex. cr
." CAN_TDH1R: " CAN_TDH1R @ hex. cr
." CAN_TI2R: " CAN_TI2R @ hex. cr
." CAN_TDT2R: " CAN_TDT2R @ hex. cr
." CAN_TDL2R: " CAN_TDL2R @ hex. cr
." CAN_TDH2R: " CAN_TDH2R @ hex. cr
." CAN_RI0R: " CAN_RI0R @ hex. cr
." CAN_RDT0R: " CAN_RDT0R @ hex. cr
." CAN_RDL0R: " CAN_RDL0R @ hex. cr
." CAN_RDH0R: " CAN_RDH0R @ hex. cr
." CAN_RI1R: " CAN_RI1R @ hex. cr
." CAN_RDT1R: " CAN_RDT1R @ hex. cr
." CAN_RDL1R: " CAN_RDL1R @ hex. cr
." CAN_RDH1R: " CAN_RDH1R @ hex. cr
." CAN_FMR: " CAN_FMR @ hex. cr
." CAN_FM1R: " CAN_FM1R @ hex. cr
." CAN_FS1R: " CAN_FS1R @ hex. cr
." CAN_FFA1R: " CAN_FFA1R @ hex. cr
." CAN_FA1R: " CAN_FA1R @ hex. cr
." CAN_F0R1: " CAN_F0R1 @ hex. cr
." CAN_F0R2: " CAN_F0R2 @ hex. cr
." CAN_F1R1: " CAN_F1R1 @ hex. cr
." CAN_F1R2: " CAN_F1R2 @ hex. cr
." CAN_F27R1: " CAN_F27R1 @ hex. cr
." CAN_F27R2: " CAN_F27R2 @ hex. cr
;

: I2C1. cr
." I2C1_CR1: " I2C1_CR1 @ hex. cr
." I2C1_CR2: " I2C1_CR2 @ hex. cr
." I2C1_OAR1: " I2C1_OAR1 @ hex. cr
." I2C1_OAR2: " I2C1_OAR2 @ hex. cr
." I2C1_TIMINGR: " I2C1_TIMINGR @ hex. cr
." I2C1_TIMEOUTR: " I2C1_TIMEOUTR @ hex. cr
." I2C1_ISR: " I2C1_ISR @ hex. cr
." I2C1_ICR: " I2C1_ICR @ hex. cr
." I2C1_PECR: " I2C1_PECR @ hex. cr
." I2C1_RXDR: " I2C1_RXDR @ hex. cr
." I2C1_TXDR: " I2C1_TXDR @ hex. cr
;

: IWDG. cr
." IWDG_KR: " IWDG_KR @ hex. cr
." IWDG_PR: " IWDG_PR @ hex. cr
." IWDG_RLR: " IWDG_RLR @ hex. cr
." IWDG_SR: " IWDG_SR @ hex. cr
." IWDG_WINR: " IWDG_WINR @ hex. cr
;

: WWDG. cr
." WWDG_CR: " WWDG_CR @ hex. cr
." WWDG_CFR: " WWDG_CFR @ hex. cr
." WWDG_SR: " WWDG_SR @ hex. cr
;

: RTC. cr
." RTC_TR: " RTC_TR @ hex. cr
." RTC_DR: " RTC_DR @ hex. cr
." RTC_CR: " RTC_CR @ hex. cr
." RTC_ISR: " RTC_ISR @ hex. cr
." RTC_PRER: " RTC_PRER @ hex. cr
." RTC_WUTR: " RTC_WUTR @ hex. cr
." RTC_ALRMAR: " RTC_ALRMAR @ hex. cr
." RTC_ALRMBR: " RTC_ALRMBR @ hex. cr
." RTC_WPR: " RTC_WPR @ hex. cr
." RTC_SSR: " RTC_SSR @ hex. cr
." RTC_SHIFTR: " RTC_SHIFTR @ hex. cr
." RTC_TSTR: " RTC_TSTR @ hex. cr
." RTC_TSDR: " RTC_TSDR @ hex. cr
." RTC_TSSSR: " RTC_TSSSR @ hex. cr
." RTC_CALR: " RTC_CALR @ hex. cr
." RTC_TAFCR: " RTC_TAFCR @ hex. cr
." RTC_ALRMASSR: " RTC_ALRMASSR @ hex. cr
." RTC_ALRMBSSR: " RTC_ALRMBSSR @ hex. cr
." RTC_BKP0R: " RTC_BKP0R @ hex. cr
." RTC_BKP1R: " RTC_BKP1R @ hex. cr
." RTC_BKP2R: " RTC_BKP2R @ hex. cr
." RTC_BKP3R: " RTC_BKP3R @ hex. cr
." RTC_BKP4R: " RTC_BKP4R @ hex. cr
." RTC_BKP5R: " RTC_BKP5R @ hex. cr
." RTC_BKP6R: " RTC_BKP6R @ hex. cr
." RTC_BKP7R: " RTC_BKP7R @ hex. cr
." RTC_BKP8R: " RTC_BKP8R @ hex. cr
." RTC_BKP9R: " RTC_BKP9R @ hex. cr
." RTC_BKP10R: " RTC_BKP10R @ hex. cr
." RTC_BKP11R: " RTC_BKP11R @ hex. cr
." RTC_BKP12R: " RTC_BKP12R @ hex. cr
." RTC_BKP13R: " RTC_BKP13R @ hex. cr
." RTC_BKP14R: " RTC_BKP14R @ hex. cr
." RTC_BKP15R: " RTC_BKP15R @ hex. cr
." RTC_BKP16R: " RTC_BKP16R @ hex. cr
." RTC_BKP17R: " RTC_BKP17R @ hex. cr
." RTC_BKP18R: " RTC_BKP18R @ hex. cr
." RTC_BKP19R: " RTC_BKP19R @ hex. cr
." RTC_BKP20R: " RTC_BKP20R @ hex. cr
." RTC_BKP21R: " RTC_BKP21R @ hex. cr
." RTC_BKP22R: " RTC_BKP22R @ hex. cr
." RTC_BKP23R: " RTC_BKP23R @ hex. cr
." RTC_BKP24R: " RTC_BKP24R @ hex. cr
." RTC_BKP25R: " RTC_BKP25R @ hex. cr
." RTC_BKP26R: " RTC_BKP26R @ hex. cr
." RTC_BKP27R: " RTC_BKP27R @ hex. cr
." RTC_BKP28R: " RTC_BKP28R @ hex. cr
." RTC_BKP29R: " RTC_BKP29R @ hex. cr
." RTC_BKP30R: " RTC_BKP30R @ hex. cr
." RTC_BKP31R: " RTC_BKP31R @ hex. cr
;

: TIM6. cr
." TIM6_CR1: " TIM6_CR1 @ hex. cr
." TIM6_CR2: " TIM6_CR2 @ hex. cr
." TIM6_DIER: " TIM6_DIER @ hex. cr
." TIM6_SR: " TIM6_SR @ hex. cr
." TIM6_EGR: " TIM6_EGR @ hex. cr
." TIM6_CNT: " TIM6_CNT @ hex. cr
." TIM6_PSC: " TIM6_PSC @ hex. cr
." TIM6_ARR: " TIM6_ARR @ hex. cr
;

: TIM7. cr
;

: DAC1. cr
." DAC1_CR: " DAC1_CR @ hex. cr
." DAC1_SWTRIGR: " DAC1_SWTRIGR @ hex. cr
." DAC1_DHR12R1: " DAC1_DHR12R1 @ hex. cr
." DAC1_DHR12L1: " DAC1_DHR12L1 @ hex. cr
." DAC1_DHR8R1: " DAC1_DHR8R1 @ hex. cr
." DAC1_DHR12R2: " DAC1_DHR12R2 @ hex. cr
." DAC1_DHR12L2: " DAC1_DHR12L2 @ hex. cr
." DAC1_DHR8R2: " DAC1_DHR8R2 @ hex. cr
." DAC1_DHR12RD: " DAC1_DHR12RD @ hex. cr
." DAC1_DHR12LD: " DAC1_DHR12LD @ hex. cr
." DAC1_DHR8RD: " DAC1_DHR8RD @ hex. cr
." DAC1_DOR1: " DAC1_DOR1 @ hex. cr
." DAC1_DOR2: " DAC1_DOR2 @ hex. cr
." DAC1_SR: " DAC1_SR @ hex. cr
;

: DAC2. cr
;

: NVIC. cr
." NVIC_ICTR: " NVIC_ICTR @ hex. cr
." NVIC_STIR: " NVIC_STIR @ hex. cr
." NVIC_ISER0: " NVIC_ISER0 @ hex. cr
." NVIC_ISER1: " NVIC_ISER1 @ hex. cr
." NVIC_ISER2: " NVIC_ISER2 @ hex. cr
." NVIC_ICER0: " NVIC_ICER0 @ hex. cr
." NVIC_ICER1: " NVIC_ICER1 @ hex. cr
." NVIC_ICER2: " NVIC_ICER2 @ hex. cr
." NVIC_ISPR0: " NVIC_ISPR0 @ hex. cr
." NVIC_ISPR1: " NVIC_ISPR1 @ hex. cr
." NVIC_ISPR2: " NVIC_ISPR2 @ hex. cr
." NVIC_ICPR0: " NVIC_ICPR0 @ hex. cr
." NVIC_ICPR1: " NVIC_ICPR1 @ hex. cr
." NVIC_ICPR2: " NVIC_ICPR2 @ hex. cr
." NVIC_IABR0: " NVIC_IABR0 @ hex. cr
." NVIC_IABR1: " NVIC_IABR1 @ hex. cr
." NVIC_IABR2: " NVIC_IABR2 @ hex. cr
." NVIC_IPR0: " NVIC_IPR0 @ hex. cr
." NVIC_IPR1: " NVIC_IPR1 @ hex. cr
." NVIC_IPR2: " NVIC_IPR2 @ hex. cr
." NVIC_IPR3: " NVIC_IPR3 @ hex. cr
." NVIC_IPR4: " NVIC_IPR4 @ hex. cr
." NVIC_IPR5: " NVIC_IPR5 @ hex. cr
." NVIC_IPR6: " NVIC_IPR6 @ hex. cr
." NVIC_IPR7: " NVIC_IPR7 @ hex. cr
." NVIC_IPR8: " NVIC_IPR8 @ hex. cr
." NVIC_IPR9: " NVIC_IPR9 @ hex. cr
." NVIC_IPR10: " NVIC_IPR10 @ hex. cr
." NVIC_IPR11: " NVIC_IPR11 @ hex. cr
." NVIC_IPR12: " NVIC_IPR12 @ hex. cr
." NVIC_IPR13: " NVIC_IPR13 @ hex. cr
." NVIC_IPR14: " NVIC_IPR14 @ hex. cr
." NVIC_IPR15: " NVIC_IPR15 @ hex. cr
." NVIC_IPR16: " NVIC_IPR16 @ hex. cr
." NVIC_IPR17: " NVIC_IPR17 @ hex. cr
." NVIC_IPR18: " NVIC_IPR18 @ hex. cr
." NVIC_IPR19: " NVIC_IPR19 @ hex. cr
." NVIC_IPR20: " NVIC_IPR20 @ hex. cr
;

: FPU. cr
." FPU_CPACR: " FPU_CPACR @ hex. cr
." FPU_FPCCR: " FPU_FPCCR @ hex. cr
." FPU_FPCAR: " FPU_FPCAR @ hex. cr
." FPU_FPDSCR: " FPU_FPDSCR @ hex. cr
." FPU_MVFR0: " FPU_MVFR0 @ hex. cr
." FPU_MVFR1: " FPU_MVFR1 @ hex. cr
;

: DBGMCU. cr
." DBGMCU_IDCODE: " DBGMCU_IDCODE @ hex. cr
." DBGMCU_CR: " DBGMCU_CR @ hex. cr
." DBGMCU_APB1FZ: " DBGMCU_APB1FZ @ hex. cr
." DBGMCU_APB2FZ: " DBGMCU_APB2FZ @ hex. cr
;

: TIM1. cr
." TIM1_CR1: " TIM1_CR1 @ hex. cr
." TIM1_CR2: " TIM1_CR2 @ hex. cr
." TIM1_SMCR: " TIM1_SMCR @ hex. cr
." TIM1_DIER: " TIM1_DIER @ hex. cr
." TIM1_SR: " TIM1_SR @ hex. cr
." TIM1_EGR: " TIM1_EGR @ hex. cr
." TIM1_CCMR1_Output: " TIM1_CCMR1_Output @ hex. cr
." TIM1_CCMR1_Input: " TIM1_CCMR1_Input @ hex. cr
." TIM1_CCMR2_Output: " TIM1_CCMR2_Output @ hex. cr
." TIM1_CCMR2_Input: " TIM1_CCMR2_Input @ hex. cr
." TIM1_CCER: " TIM1_CCER @ hex. cr
." TIM1_CNT: " TIM1_CNT @ hex. cr
." TIM1_PSC: " TIM1_PSC @ hex. cr
." TIM1_ARR: " TIM1_ARR @ hex. cr
." TIM1_RCR: " TIM1_RCR @ hex. cr
." TIM1_CCR1: " TIM1_CCR1 @ hex. cr
." TIM1_CCR2: " TIM1_CCR2 @ hex. cr
." TIM1_CCR3: " TIM1_CCR3 @ hex. cr
." TIM1_CCR4: " TIM1_CCR4 @ hex. cr
." TIM1_BDTR: " TIM1_BDTR @ hex. cr
." TIM1_DCR: " TIM1_DCR @ hex. cr
." TIM1_DMAR: " TIM1_DMAR @ hex. cr
." TIM1_CCMR3_Output: " TIM1_CCMR3_Output @ hex. cr
." TIM1_CCR5: " TIM1_CCR5 @ hex. cr
." TIM1_CCR6: " TIM1_CCR6 @ hex. cr
." TIM1_OR: " TIM1_OR @ hex. cr
;

: ADC1_2. cr
." ADC1_2_ISR: " ADC1_2_ISR @ hex. cr
." ADC1_2_IER: " ADC1_2_IER @ hex. cr
." ADC1_2_CR: " ADC1_2_CR @ hex. cr
." ADC1_2_CFGR: " ADC1_2_CFGR @ hex. cr
." ADC1_2_SMPR1: " ADC1_2_SMPR1 @ hex. cr
." ADC1_2_SMPR2: " ADC1_2_SMPR2 @ hex. cr
." ADC1_2_TR1: " ADC1_2_TR1 @ hex. cr
." ADC1_2_TR2: " ADC1_2_TR2 @ hex. cr
." ADC1_2_TR3: " ADC1_2_TR3 @ hex. cr
." ADC1_2_SQR1: " ADC1_2_SQR1 @ hex. cr
." ADC1_2_SQR2: " ADC1_2_SQR2 @ hex. cr
." ADC1_2_SQR3: " ADC1_2_SQR3 @ hex. cr
." ADC1_2_SQR4: " ADC1_2_SQR4 @ hex. cr
." ADC1_2_DR: " ADC1_2_DR @ hex. cr
." ADC1_2_JSQR: " ADC1_2_JSQR @ hex. cr
." ADC1_2_OFR1: " ADC1_2_OFR1 @ hex. cr
." ADC1_2_OFR2: " ADC1_2_OFR2 @ hex. cr
." ADC1_2_OFR3: " ADC1_2_OFR3 @ hex. cr
." ADC1_2_OFR4: " ADC1_2_OFR4 @ hex. cr
." ADC1_2_JDR1: " ADC1_2_JDR1 @ hex. cr
." ADC1_2_JDR2: " ADC1_2_JDR2 @ hex. cr
." ADC1_2_JDR3: " ADC1_2_JDR3 @ hex. cr
." ADC1_2_JDR4: " ADC1_2_JDR4 @ hex. cr
." ADC1_2_AWD2CR: " ADC1_2_AWD2CR @ hex. cr
." ADC1_2_AWD3CR: " ADC1_2_AWD3CR @ hex. cr
." ADC1_2_DIFSEL: " ADC1_2_DIFSEL @ hex. cr
." ADC1_2_CALFACT: " ADC1_2_CALFACT @ hex. cr
." ADC1_2_CSR: " ADC1_2_CSR @ hex. cr
." ADC1_2_CCR: " ADC1_2_CCR @ hex. cr
." ADC1_2_CDR: " ADC1_2_CDR @ hex. cr
;

: SYSCFG. cr
." SYSCFG_CFGR1: " SYSCFG_CFGR1 @ hex. cr
." SYSCFG_EXTICR1: " SYSCFG_EXTICR1 @ hex. cr
." SYSCFG_EXTICR2: " SYSCFG_EXTICR2 @ hex. cr
." SYSCFG_EXTICR3: " SYSCFG_EXTICR3 @ hex. cr
." SYSCFG_EXTICR4: " SYSCFG_EXTICR4 @ hex. cr
." SYSCFG_CFGR2: " SYSCFG_CFGR2 @ hex. cr
." SYSCFG_RCR: " SYSCFG_RCR @ hex. cr
;

: OPAMP. cr
." OPAMP_OPAMP1_CR: " OPAMP_OPAMP1_CR @ hex. cr
." OPAMP_OPAMP2_CR: " OPAMP_OPAMP2_CR @ hex. cr
." OPAMP_OPAMP3_CR: " OPAMP_OPAMP3_CR @ hex. cr
." OPAMP_OPAMP4_CR: " OPAMP_OPAMP4_CR @ hex. cr
;

