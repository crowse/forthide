#  Build lists of words based on naming conventions

#  The list entry consists of a name and 5 sub lists
#      ['name', [general words], [immediate words],
#               [internal words],[status words],  [variables]]
#  Some words are added to more than one list
#       this facilitates display on small devices
# It may be best to add the list names to the menu
from tkinter import *
from tkinter import filedialog
from tkinter import messagebox
from tkinter.filedialog import askopenfilename
from tkinter.ttk import *


class dictionary:
    wordList=[]
    
    def addWords(io):
        io.write("___listX\r\n")
        io.flush()
        sleep(1.00)
        strx = str(io.readline())
        wordList[0] = ['--- Forth Words ---',[],[],[],[],[]]
        print("Loading Words",strx)
        column = 0
        while (strx != ''): 
            if (strx.startswith('---')):    # new dictionary
                column+=1
                wordList[column][0] = strx
            elif (strx.startswith('!!!')):  # Execute immediately in debugger
                wordList[column][2].insert(END,strx)
            elif (strx.startswith('___')):  # internal/hidden word
                wordList[column][3].insert(END,strx)
            elif (strx.startswith('.')):    # status words
                wordList[column][4].insert(END,strx)
            elif (strx.startswith('@!@!')): # variable
                wordList[column][5].insert(END,strx[4,END])
            else:                          # normal word
                wordList[column][1].insert(END,strx)
        return wordList

    def defineListX(io):
              io.write(': listX  cr dictionarystart begin dup ( dup cfa @ $xxxx = if ." @!@!" then ) 6 + ctype CR dictionarynext until drop ;')
              io.flush

    def findVocabulary(name):
        for voc in wordList:
            if (voc[0] == name):
                return voc
        return [name,[],[],[],[],[]]
   # ******************************************************* main
if __name__ == '__main__':
   root = Tk()
   root.title("Shorai Forth IDE")
   body=Frame(root)
   root.textArea =  Text(body)

   root.forthLine = StringVar() 
   root.status= StringVar()
   displayWelcome() # root.status.set(displayText)
   root.historyValues = []
   
   ents = makeform(root, fields)
   buttons1=makeButtons(root,buttons)
   buttons2=makeButtons(root,buttons2)
   makeHistory(root)
   makeForth(root)
   
   area = Frame(body)
   #buttonsArea = Frame(area)
   #listArea = Frame(buttonsArea)
   calc0 = Frame(area)
   calc1 = Frame(area)

   calcs = Frame(calc0)
   wpp0 = Frame(calc0)
   wpp1 = Frame(calc1)
   wpp2 = Frame(calc1)
   
   cb=calcButtons(calcs)
   root.wordList = []
   root.wordList.append([])
   root.wordList.append([])
   root.wordList.append([])
   
   root.wordList=getList(root.serialIO,root.wordList)
   
   wp0 = wordPicker(wpp0,0,root.wordList[0])
   wp1 = wordPicker(wpp1,1,root.wordList[1])
   wp2 = wordPicker(wpp2,2,root.wordList[2])

   calcs.pack(side=LEFT)
   wpp0.pack(side=RIGHT,expand=YES,fill=Y)
   wpp1.pack(side=LEFT)
   wpp2.pack(side=RIGHT)
   
   calc0.pack(anchor='n')
   calc1.pack(anchor='s')
   
   #textArea =  Message(area, textvariable=root.status,anchor='e')
   #root.textArea =  ScrolledText(area)
   
   
   root.textArea.config(bg='lightgreen', font=('times', 12, 'normal'))
   displayWelcome() # root.textArea.insert(END,displayText)
   root.textArea.pack(side=RIGHT,expand=YES,fill=X)

   area.pack(side=LEFT)
   body.pack(expand=YES,fill=X)   
   b1 = Button(root, text='Show',
          command=(lambda e=ents: fetch(e)))
   b1.pack(side=LEFT, padx=5, pady=5)
   b2 = Button(root, text='Quit', command=root.quit)
   b2.pack(side=LEFT, padx=5, pady=5)
   
   #readResponse()
   t1 = threading.Thread(target=worker, args=[])
   t1.start()

   root.mainloop()

   

root.mainloop()

         
