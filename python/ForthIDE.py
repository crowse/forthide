#!py
#!/usr/bin/py
#   built for Python 3
from tkinter import *
from tkinter import filedialog
from tkinter import messagebox
from tkinter.filedialog import askopenfilename
from tkinter.ttk import *
import serial
import threading
import time
import glob
import io
from time import sleep

from serialConnection import *


# Main axis positioning in absolute or relative
# Change the FG and BG colours so we always know what we are doing!!!
fields = 'XTarget', 'YTarget', 'ZTarget', 'FeedRate','Spindle','Laser'

# actions
buttons =  'XYZ!', 'dXYZ', 'Drill', 'GoHome', 'GoClear', 'Arm Laser','Setup', 'Go'
#status
buttons2 = '.state','.calcs','.stack', 'Stop'

# Setup commands in a popup dialog
settings='FindHome', 'SetOffset','SetZero','Connect'

# *********************************************************************** Serial port stuff
def getConnection():
    sc = serialConnection()
    if sys.platform.startswith('linux'):
         #name="/dev/ttyACM0"
         name="/dev/ttyUSB1"
         
    elif sys.platform.startswith('win'):
        name="COM10"
    try :    
        sc.tryConnect(name)
    except Exception as e:
        print("Failed to open "+name + " Reason:??", e)
    return sc

def send(command):
    # refreshHistory(command)
    serial.tx(command+"\r")
    root.rxComplete = 0
    
def receiver(textArea):
        strx = str(serial.readline())
        #if (strx != ""):
        #    print("Rx:"+strx+ "<--")
        if "ok." in strx:
            # print ("Rx: string contains ok.",strx)
            root.rxComplete = -1
        if ((strx != "") & (strx != "\r") & (strx != "\r\n") & (strx != "\n")):
            print(strx.rstrip())
            textArea.insert(END,strx)
            textArea.see(END)
        
        
def getListX(io,wordList):
    io.write("___listX\r\n")
    io.flush()
    sleep(1.00)
    strx = str(io.readline())
    wordList[0] = ['--- Forth Words ---',[],[],[],[],[]]
    print("Loading Words",strx)
    column = 0
    while (strx != ''): 
        if (strx.startswith('---')):    # new dictionary
            column+=1
            wordList[column][0] = strx
        elif (strx.startswith('!!!')):  # Execute immediately in debugger
            wordList[column][2].insert(END,strx)
        elif (strx.startswith('___')):  # internal/hidden word
            wordList[column][3].insert(END,strx)
        elif (strx.startswith('.')):    # status words
            wordList[column][4].insert(END,strx)
        elif (strx.startswith('@!@!')): # variable
            wordList[column][5].insert(END,strx[4,END])
        else:                          # normal word
            wordList[column][1].insert(END,strx)
                

            
def getList(serial,wordList):        
    send("list")
    sleep(1.00)
    strx = str(serial.readline())
    wordList[0] = []
    print("Loading Words",strx)
    column = 0
    state = 0
    while (strx != ''): 
        words = strx.split()
        for word in words:
            if (word == '---'):
                if (state == 3):
                    column += 1
                    wordList[column] = []
                    state=0
                    print("Start column:",column)
                else:
                    state +=1
            elif (word == "Mecrisp-Stellaris"):
                state += 1
            elif (word == 'Core') & (state == 2):
                  state += 1
            elif (word == 'Flash') & (state == 1):
                  state += 1
            elif (word == 'Dictionary') & (state == 2):
                  state += 1
            elif (word != "ok."):
                #wordList.insert('end',word)
                wordList[column].append(word)
                #print(state,column,word)
        strx = serial.readline()
    wordList[0] = sorted(wordList[0])
    wordList[1] = sorted(wordList[1])
    wordList[2] = sorted(wordList[2])
    
    return wordList

def refreshHistory(command):
    try:
        #root.historyBox.remove(command)
        root.historyValues.remove(command)
    except:
        pass
    root.historyValues.insert(0,command)
    root.historyBox['values']=root.historyValues
    root.historyBox.set(command)
    #root.historyBox.insert(0,command)

    

def waitDone():
    while(-1):
        if(root.rxComplete == 0):
           return
        if (textArea.value().endsWith("ok.")):
            return
    return root.rxComplete

# ******************************************************* Actions


# -------------------- Data Entry fields
def fetch(widg,name,editBox):
      displayText = 'Show:\r\n'
      #for entry in entries:
      steps = 0
      text = editBox.get()
      try :
          val = float(text)
          if ((name=='X') |(name=='Y') | (name=='Z')):
              steps=val*25600.0/3.0
          else:
              steps = val
          print('%s: "%s" Steps:%9d' % (name, text,steps))
          displayText += name
          displayText += '\t'
          displayText += text
          displayText += '  Steps:'
          displayText += str(steps)
          displayText += '\r\n'
          editBox.config(fg = 'black')
      except ValueError:
          print(name + " ValueError :" + text + ":")
          editBox.config(fg='red')
          root.textArea.insert(END,"\nValue Error "+ text)
    
def actions(actions):
    for action in actions:
        if (action=='Go'):
            print("Go Pressed")
        else:
            if (action=='Relative'):
                print("Relative")
            else:
                print("Button was " + action)

def doForth(event):
     doForth1()
     
def doForth1():
    str = root.forthLine.get()
    refreshHistory(str)
    send(str)
    root.forthLine.set("")

def doHistory(event):
     doHistory1()
     
def doHistory1():
    str = root.historyBox.get() 
    serial.Tx(str)
  




def numberButton(event):
    txt = event.widget['text']
    if (txt in '0123456789+.'):
        root.forthLineEdit.insert(INSERT,txt)
    elif (txt == '_'):
        root.forthLineEdit.insert(INSERT,' ')
    else:
        root.forthLineEdit.insert(INSERT," "+txt)

    
def wordButton(event):
    root.forthLineEdit.insert(INSERT," " +event.widget['text']) 
    
    
def variableButton(widg,name,entry):
    root.forthLineEdit.insert(INSERT," " +entry.get()+ " " + name +"!") 

# There are 3 dictionaries latest, --- Mecrisp-Stellaris Core ---, --- Flash Dictionary ---
def addListedWord(self,column,data):
    items = self.widget.curselection()
    #items = [self.widget.data[int(item)] for item in items]
    items = [self.widget.data[item] for item in items]
    for item in items:
        root.forthLineEdit.insert(INSERT," " +item)


# ***************************************************************** MEnu
def donothing():
    return
def load_filex():
    load_file(root)


def doFile(fname):
    try:
        print("doFile: " + fname)
        with open(fname) as fp:
            lines = fp.read().splitlines()
            
            for line in lines:
                line = line.strip()
                #if ((line.startswith("#include")) | (line.startswith("#require"))):
                #    xx = line.split()
                #    doFile(xx[1])
                #if (line.startswith("\\") is False):
                #    send(line)
                #    waitDone()
    except Exception as e:                      # <- nak'ed except is a bad idea
        print("doFile - Failed to read file\n'%s' %s", fname, e)
    return

def load_file():
        fname = askopenfilename(filetypes=[("Forth files",("*.forth","*.fs","*.4th","*.txt")),
                                           ("Text files","*.txt"),
                                           ("All files", "*.*")])
        if fname:
            doFile(fname)

def display_file(root,fname):
        if fname:
            try:
                print("Displaying: " + fname)
                root.textArea.delete('1.0',END)
                root.textArea.insert(END,fname)
                with open(fname) as fp:
                    lines = fp.read()
                for line in lines:    
                     root.textArea.insert(END,line)
                #root.textArea.see(START)                   
            except Exception as e:                      # <- naked except is a bad idea
                print("Display File - Failed to read file\n" +  fname + " " + e)
            return


def displayAbout():
    display_file(root,"About.txt")

def displaySetup():
    display_file(root,"Setup.txt")

def displayTodo():
    display_file(root,"todo.txt")

def displayUsage():
    display_file(root,"UsageTips.txt")

def displayWelcome():
    display_file(root,"Welcome.txt")

def clearTextArea():
    root.textArea.delete('1.0',END)
    
def addMenu(window):
    menubar = Menu(window)
    filemenu = Menu(menubar, tearoff=0)
    #filemenu.add_command(label="New", command=donothing)
    #filemenu.add_command(label="Open", command=donothing)
    #filemenu.add_command(label="Save", command=donothing)
    #filemenu.add_command(label="Save as...", command=donothing)
    #filemenu.add_command(label="Close", command=donothing)
    filemenu.add_command(label="Send", command=load_file)

    filemenu.add_separator()

    filemenu.add_command(label="Exit", command=root.quit)
    menubar.add_cascade(label="File", menu=filemenu)

    editmenu = Menu(menubar, tearoff=0)
    editmenu.add_command(label="Clear", command=clearTextArea)

    editmenu.add_separator()

    editmenu.add_command(label="Cut", command=donothing)
    editmenu.add_command(label="Copy", command=donothing)
    editmenu.add_command(label="Paste", command=donothing)
    editmenu.add_command(label="Delete", command=donothing)
    editmenu.add_command(label="Select All", command=donothing)
    editmenu.add_command(label="Undo", command=donothing)

    menubar.add_cascade(label="Edit",menu=editmenu)

    settingsmenu = Menu(menubar, tearoff=0)
    settingsmenu.add_command(label="Connect", command=getConnection)

    menubar.add_cascade(label="Settings", menu=settingsmenu)
    helpmenu = Menu(menubar, tearoff=0)

    helpmenu.add_command(label="About",command=displayAbout)
    helpmenu.add_command(label="Setup",command=displaySetup)
    helpmenu.add_command(label="To Do",command=displayTodo)
    helpmenu.add_command(label="Usage and Tips",command=displayUsage)
    helpmenu.add_command(label="Welcome",command=displayWelcome)
    
    # helpmenu.add_command(label=": list",command=colonList)
    
    menubar.add_cascade(label="Help", menu=helpmenu)
   
    return menubar
# *******************************************************Panels and  Widgets     
#  For fields commands as above
def makeform(root, fields):
   entries = []
   row = Frame(root)
   row2= Frame(root)
   for field in fields:
      lab = Button(row2,  width=15, text=field) #, anchor='center')
      ent = Entry(row,width=15, text=field)
      ef=(field,ent)
 
      ent.bind('<FocusOut>',lambda event, name=field, box=ent: fetch(event,name,box))
      lab.bind('<Button-1>',lambda event, name=field, box=ent: variableButton(event,name,box))
      ent.insert(10,'0')
      lab.pack(side=LEFT, expand=YES, fill=X)
      ent.pack(side=LEFT, expand=YES, fill=X)
      entries.append(ef)
   row.pack(side=TOP, fill=X,  padx=2, pady=2)
   row2.pack(side=TOP, fill=X, padx=2, pady=2)
   return entries

# Forth command line for entering commands
def makeForth(root):
   row = Frame(root)
   ent = Entry(row,width=80, text=root.forthLine)
   root.forthLineEdit = ent
   but = Button(row, text="Send Forth",command=doForth1)
   ent.bind("<Return>",doForth)
   ent.pack(side=LEFT, expand=YES, fill=X)
   but.pack(side=RIGHT, expand=NO, fill=X)
   row.pack(side=TOP, fill=X,  padx=5, pady=5)
   return ent

def makeHistory(root):
    panel=Frame(root)
    h = Combobox(panel,width=80,values=root.historyValues)
    root.historyBox = h
    but = Button(panel, text="Redo:",command=doHistory1)
    h.bind("<Return>",doHistory)
    h.pack(side=LEFT, expand=YES, fill=X)
    but.pack(side=RIGHT, expand=NO, fill=X)
    panel.pack(side=TOP, fill=X,  padx=5, pady=5)
    return panel

# general purpose buttons
def makeButtons(root, fields):
	entries = []
	row = Frame(root)
	for field in fields:
		but = Button(row,   text=field) #, anchor='w')
		but.bind('<Button-1>',wordButton)
		but.pack(side=LEFT, expand=YES, fill=X)
		entries.append((field,but))
	row.pack(side=TOP, fill=X,  padx=5, pady=5)
	return entries

def calcButtons(calc):
    line1='7','8','9'
    line2='4','5','6'
    line3='1','2','3'
    line4='_','0','Go'
    line5='del','space','Bksp'
    wth=5
    ht=3
    entries = []
    row1= Frame(calc)
    for field in line1:
            but = Button(row1,  text=field) # , width=wth, height=ht) #anchor='center'
            but.bind('<Button-1>',numberButton)
            but.pack(side=LEFT, expand=YES, fill=X)
            entries.append((field,but))
    row1.pack(side=TOP, fill=X,  padx=5, pady=5)

    row2= Frame(calc)
    for field in line2:
        but = Button(row2,   text=field) #width=wth, height=ht,, anchor='center')
        but.bind('<Button-1>',numberButton)
        but.pack(side=LEFT, expand=YES, fill=X)
        entries.append((field,but))
    row2.pack(side=TOP, fill=X,  padx=5, pady=5)

    row3= Frame(calc)
    for field in line3:
          but = Button(row3,text=field) #width=wth, height=ht, anchor='center')
          but.bind('<Button-1>',numberButton)
          but.pack(side=LEFT, expand=YES, fill=X)
          entries.append((field,but))
    row3.pack(side=TOP, fill=X,  padx=5, pady=5)

    row4= Frame(calc)
    for field in line4:
              but = Button(row4,  text=field) # width=wth, height=ht,, anchor='center')
              but.bind('<Button-1>',numberButton)
              but.pack(side=LEFT, expand=YES, fill=X)
              entries.append((field,but))
    row4.pack(side=TOP, fill=X,  padx=5, pady=5)
    calc.pack(anchor='w')
    
    return entries


def wordPicker(wl,column,data):
    print("wordPicker",column,len(data))
    scrollbar = Scrollbar(wl, orient=VERTICAL)
    listbox = Listbox(wl, yscrollcommand=scrollbar.set)
    listbox.insert(END,*data)
    scrollbar.config(command=listbox.yview)
    scrollbar.pack(side=RIGHT, fill=Y)
    listbox.pack(side=LEFT, fill=BOTH, expand=1)
    listbox.bind('<Double-Button-1>',lambda event, column=column: addListedWord(event,column,data))
    listbox.data = data
    #print(str(data))
    return wl

#def mainWindow(root):
   
def worker():
    # Skeleton worker function, runs in separate thread (see below)   
    while True:
        #serial.rx()
        receiver(root.textArea)
        time.sleep(0.05)



def colonList():
    send(": list  cr dictionarystart begin dup 6 + ctype space dictionarynext until drop ;") 
    getList()
    
# ******************************************************* main
if __name__ == '__main__':
   root = Tk()
   root.title("Shorai Forth IDE")
   body=Frame(root)
   root.textArea =  Text(body)
   menubar=addMenu(root)
   root.config(menu=menubar)
   #tryConnect()
   serial = getConnection()
   #mainWindow(root)

   root.forthLine = StringVar() 
   root.status= StringVar()
   displayWelcome() # root.status.set(displayText)
   root.historyValues = []
   
   ents = makeform(root, fields)
   buttons1=makeButtons(root,buttons)
   buttons2=makeButtons(root,buttons2)
   makeHistory(root)
   makeForth(root)
   
   area = Frame(body)
   #buttonsArea = Frame(area)
   #listArea = Frame(buttonsArea)
   calc0 = Frame(area)
   calc1 = Frame(area)

   calcs = Frame(calc0)
   wpp0 = Frame(calc0)
   wpp1 = Frame(calc1)
   wpp2 = Frame(calc1)
   
   cb=calcButtons(calcs)
   root.wordList = []
   root.wordList.append([])
   root.wordList.append([])
   root.wordList.append([])
   
   root.wordList=getList(serial,root.wordList)
   
   wp0 = wordPicker(wpp0,0,root.wordList[0])
   wp1 = wordPicker(wpp1,1,root.wordList[1])
   wp2 = wordPicker(wpp2,2,root.wordList[2])

   calcs.pack(side=LEFT)
   wpp0.pack(side=RIGHT,expand=YES,fill=Y)
   wpp1.pack(side=LEFT)
   wpp2.pack(side=RIGHT)
   
   calc0.pack(anchor='n')
   calc1.pack(anchor='s')
   
   #textArea =  Message(area, textvariable=root.status,anchor='e')
   #root.textArea =  ScrolledText(area)
   
   
   root.textArea.config(bg='lightgreen', font=('times', 12, 'normal'))
   displayWelcome() # root.textArea.insert(END,displayText)
   root.textArea.pack(side=RIGHT,expand=YES,fill=X)

   area.pack(side=LEFT)
   body.pack(expand=YES,fill=X)   
   b1 = Button(root, text='Show',
          command=(lambda e=ents: fetch(e)))
   b1.pack(side=LEFT, padx=5, pady=5)
   b2 = Button(root, text='Quit', command=root.quit)
   b2.pack(side=LEFT, padx=5, pady=5)
   
   #readResponse()
   t1 = threading.Thread(target=worker, args=[])
   t1.start()

   root.mainloop()

   

root.mainloop()


    
