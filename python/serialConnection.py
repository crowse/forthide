import serial
import glob
import sys
import io
from tkinter import *
from time import sleep

class serialConnection:
    
# ****************************************************** Serial port
    def serial_ports(self):
        """ Lists serial port names
               :raises EnvironmentError:
                      On unsupported or unknown platforms
               :returns:
                      A list of the serial ports available on the system
        """
        if sys.platform.startswith('win'):
              ports = ['COM%s' % (i + 1) for i in range(256)]
        elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
                # this excludes your current terminal "/dev/tty"
                ports = glob.glob('/dev/tty[A-Za-z]*')
        elif sys.platform.startswith('darwin'):
             ports = glob.glob('/dev/tty.*')
        else:
             raise EnvironmentError('Unsupported platform')

        result = []
        for port in ports:
             try:
                s = serial.Serial(port)
                s.close()
                result.append(port)
             except (OSError, serial.SerialException):
                pass
        return result

    def openPort(self,name): #todo: get a list of ports and let the user choose which one to try 
        print("Trying to open port:" + name)
        self.serialPort = serial.Serial(name,115200,timeout=0.2)        
        self.serialIO = io.TextIOWrapper(io.BufferedRWPair(self.serialPort, self.serialPort))
        return self.serialIO

    def listPorts(self):
        ports = self.serial_ports() 
        for p in ports:
                print(p)
        print("Ports" + str(ports))
        return ports

    def firstPort(self):
        ports = self.listPorts()
        for p in ports:
            print ("Trying port:" ,p)
            try:
                self.conn = self.openPort(p)
                print("Opened port from list:" + p)
                self.conn.write("\r\n")
                self.conn.flush()
                sleep(0.200)
                strx = str(self.conn.readline())
                return strx 
            except Exception as e:
                print("Failed to use port",e)
                self.conn=0
        return ''
        
    def tryConnect(self,name):
        self.conn=0
        
        #while (self.conn==0):
        try:
            self.conn = self.openPort(name)
            print("Opened port:" + name)
            self.conn.write("\r\n")
            self.conn.flush()
            sleep(0.200)
            strx = str(self.conn.readline())
        except Exception as e:
            print("Failed to use port",e)
            self.conn=0
        if (self.conn==0):
            strx=self.firstPort()
        while (strx != ''): 
            print(strx)
            strx = str(self.conn.readline())
                
                
    def tx(self,command):
        #refreshHistory(command)
        self.serialIO.write(command+"\n")
        self.serialIO.flush()
        print ("TX:",command)
        


    def readResponse(area):
        len = self.serialPort.in_waiting
        if (len >0):
            strx=ser.read(len)
            area.insert(END,strx)

    def rx(textArea):
        strx = str(self.serialIO.readline())
        if (strx.endswith("ok.")):
            rxComplete = true
        if ((strx != "") & (strx != "\r") & (strx != "\r\n") & (strx != "\n")):
            print(strx.rstrip())
            textArea.insert(END,strx)
            textArea.see(END)

    def readline(self):
        return self.serialIO.readline()

# ******************************************************* main
if __name__ == '__main__':
    root = Tk()
    sc = serialConnection()
    ports = sc.listPorts()
    #sc.tryConnect("/dev/ttyACM0")
    if sys.platform.startswith('linux'):
         name="/dev/ttyACM0"
    elif sys.platform.startswith('win'):
        name="COM10"
    try :    
        sc.tryConnect(name)
    except Exception as e:
        print("Failed to open "+name + " Reason:??", e )
    #root.mainloop()
    
    
   



